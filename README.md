This repository contains the Kafka consumers.

# Set up: #
Pull from git
Using Intellij, open the parent folder as a project (folder based).
Using maven, install "IngestionConsumer" module.
The output will be in ROOTFOLDER/IngestionConsumer/target - Copy target/ingestionconsumer.jar to {target dir}, and target/alternateLocation/* to {target dir}/lib/

*Target server requires JRE1.7*

# Running: #
**For the default command line to run all consumers, see scripts/runConsumer.sh**

There are 2 sets of consumers, depending on their destination.
MRP: MRPTrackerConsumer, MRPAdvTrackerConsumer, MobileConsumer, IosConsumer
FTP: FTPTrackerConsumer, FTPAdvTrackerConsumer, MobileHistoryConsumer

All of them use the default hadoop configuration to know the HDFS location. To make sure, check /etc/hadoop/conf/hdfs-site.xml and make sure that the correct HDFS URL is set there.

To run a consumer, run from the {target dir}:

**sudo java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.{ConsumerName}**

If there are multiple HDFS configurations on the same machine, it's possible to run with a specific HDFS config:

**sudo java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.{ConsumerName} -c {config}**

Where config is the path to the correct config, such as /etc/hadoop/conf.mrp

# Configuration: #
In ETCD, under /v1/production/services/data-ingestion/kafka-consumers
Here there are config keys for each consumer and utility. If you need to change the default config path, for example, it is possible to change the key "/v1/production/services/data-ingestion/kafka-consumers/hdfsutil/default-conf-dir"

# Logs: #
[http://elk.sg.internal:9200/_plugin/head/](http://elk.sg.internal:9200/_plugin/head/)
In Structured Query, choose the correct index (with the wanted date), and change "match_all" to "logs.facility.raw", put "KafkaConsumer" as the term