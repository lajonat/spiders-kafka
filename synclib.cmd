@echo off
FOR /F "usebackq tokens=*" %%a IN (`cygpath -u %~dp0`) DO set comm=%%asynclib.sh

FOR /F "usebackq tokens=*" %%a IN (`cygpath -u %1`) DO set targetpath=%%a

bash -c "%comm% -f %targetpath% -s %2 -b"
