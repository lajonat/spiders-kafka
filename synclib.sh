#!/bin/sh

TARGETFOLDER=similargroup/dev/
SERVER=runsrv
FILES=package/*.tar.gz

while [[ $# > 0 ]]
do
key="$1"
shift
case $key in
    -t|--targetfolder)
    TARGETFOLDER="$1"
    shift
    ;;
    -b|--branchtargetfolder)
    TARGETFOLDER="similargroup_$(git rev-parse --abbrev-ref HEAD)"
    #shift
    ;;
    -s|--server)
    SERVER="$1"
    shift
    ;;
    -f|--files)
    FILES="$1"
    shift
    ;;
    *)
    echo "USAGE: ${BASH_SOURCE[0]}"
    echo "  [-f,--files <files to sync>] - Default: all 'package' folder contents"
    echo "  [-s,--server <server to deploy to>] - Default: 'runsrv'"
    echo "  [-t,--targetfolder <target folder>] - Default: 'similargroup/dev/'"
    exit 2
    ;;
esac
done

old=`pwd`

set -e # Exit on error

for f in ${FILES}
do
  dir=$(dirname ${f})
  file=$(basename ${f})
  pushd ${dir} > /dev/null
  prefix=$(echo ${file} | cut -d'.' -f 1)
  mkdir -p /tmp/synclib/${prefix}
  echo "Unpacking tarball..."
  rm -rf /tmp/synclib/${prefix}/*
  tar -xvf ${file} -C /tmp/synclib/${prefix} > /dev/null
  ssh ${SERVER} mkdir -p ${TARGETFOLDER} #/${prefix}
  echo "Sending deltas to ${SERVER}:${TARGETFOLDER} ..."
  rsync --delete-delay --progress -rtvz /tmp/synclib/${prefix}/* ${SERVER}:${TARGETFOLDER}
  echo "Cleaning up..."
  echo "Changing premissions for shell scripts"
  rm -rf /tmp/synclib/${prefix}
  echo "ssh ${SERVER} 'find  ./${TARGETFOLDER}/${prefix} -name "*.sh" -exec chmod +x {} +'"
  ssh ${SERVER} "find  ./${TARGETFOLDER}/${prefix} -name '*.sh' -exec chmod +x {} +"
  echo "ssh ${SERVER} 'find  ./${TARGETFOLDER}/bin -exec chmod +x {} +'"
  ssh ${SERVER} "find  ./${TARGETFOLDER}/bin -exec chmod +x {} +"
  echo "ssh ${SERVER} 'find  ./${TARGETFOLDER}/scripts -name "*.sh" -exec chmod +x {} +'"
  ssh ${SERVER} "find  ./${TARGETFOLDER}/scripts -name '*.sh' -exec chmod +x {} +"
  echo "done."
  popd > /dev/null
done

