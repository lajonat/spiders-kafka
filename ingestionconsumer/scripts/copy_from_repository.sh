# this is a script we use on tstage2 to copy the relevant scripts after build

cp -rf ../kafka-consumers-repository/ingestionconsumer/target/alternateLocation/* lib
cp -f ../kafka-consumers-repository/ingestionconsumer/target/ingestionconsumer.jar .
cp -f ../kafka-consumers-repository/ingestionconsumer/scripts/runConsumers.sh . 
cp -f ../kafka-consumers-repository/ingestionconsumer/scripts/killConsumers.sh . 
