#!/bin/bash

java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.FTPAdvTrackerConsumer -c /etc/hadoop/conf.ftpfs-hadoop/ >> ftpadvtracker.log 2>&1 &
java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.FTPTrackerConsumer -c /etc/hadoop/conf.ftpfs-hadoop/ >> ftptracker.log 2>&1 &
java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.MobileHistoryConsumer -c /etc/hadoop/conf.ftpfs-hadoop/ >> mobilehistory.log 2>&1 &

java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.MobileConsumer -c /etc/hadoop/conf.mrp-hadoop/ >> mobile.log 2>&1 &
java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.MRPAdvTrackerConsumer -c /etc/hadoop/conf.mrp-hadoop/ >> mrpadvtracker.log 2>&1 &
java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.MRPTrackerConsumer -c /etc/hadoop/conf.mrp-hadoop/ >> mrptracker.log 2>&1 &
java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.IosConsumer -c /etc/hadoop/conf.mrp-hadoop/ >> ios.log 2>&1 &
