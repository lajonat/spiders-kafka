#!/bin/bash

java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.FTPAdvTrackerConsumer -c /etc/hadoop/conf.ftpfs-hadoop/ -e staging 2&> logs/FTPAdv.log &
java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.FTPTrackerConsumer -c /etc/hadoop/conf.ftpfs-hadoop/ -e staging 2&> logs/FTP.log &
java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.MobileHistoryConsumer -c /etc/hadoop/conf.ftpfs-hadoop/ -e staging 2&> logs/MobileHistory.log &

java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.IosConsumer -c /etc/hadoop/conf.ftpfs-hadoop/ -e staging 2&> logs/Ios.log &
java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.MobileConsumer -c /etc/hadoop/conf.ftpfs-hadoop/ -e staging 2&> logs/Mobile.log &
java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.MRPAdvTrackerConsumer -c /etc/hadoop/conf.ftpfs-hadoop/ -e staging 2&> logs/MRPAdv.log &
java -cp ingestionconsumer.jar:lib/* com.similargroup.DIConsumer.MRPTrackerConsumer -c /etc/hadoop/conf.ftpfs-hadoop/ -e staging 2&> logs/MRP.log &
