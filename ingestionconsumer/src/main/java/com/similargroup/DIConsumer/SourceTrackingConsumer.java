package com.similargroup.DIConsumer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.similargroup.DIConsumer.base.BaseProcessor;
import com.similargroup.DIConsumer.base.KafkaMessageException;
import com.similargroup.DIConsumer.utils.*;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.util.ShutdownHookManager;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.msgpack.jackson.dataformat.MessagePackFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
* Reads tracker messages, transforms to TSV - SAVE TO MRP
* Created by jonathan on 5/06/2015.
*/
public class SourceTrackingConsumer {

	private static Logger log = Logger.getLogger("kafkalog");

	private static KafkaListener listener;

	private static final String GROUP_ID= "sourcetracker";
	private static final String OUTPUT_MONGO= "mongo";
	private static final String OUTPUT_HDFS= "hdfs";
	private static final String OUTPUT_BOTH= "both";

	private static final String MONGO_DB = "spiders-prod";
	private static final String MONGO_USER = "spider";
	private static final String MONGO_PASS = "93beb63423dfc350f7a4a7b9e9cca5ba";
	private static final String MONGO_CONN = "mongodb://" + MONGO_USER + ":" + MONGO_PASS + "@candidate.43.mongolayer.com:10033,candidate.40.mongolayer.com:10351/" + MONGO_DB;
	private static final String MONGO_COLLECTION = "debugconsumer";


	private static class Arguments {
		@Argument(alias = "c", description = "Path to HDFS config dir")
		String config;
		@Argument(alias = "e", description = "Environment to run in (default is production, other option is staging)")
		String env = "production";
		@Argument(alias = "t", description = "Which topic to listen to", required = true)
		String topic;
		@Argument(alias = "s", description = "Source to filter")
		String source;
		@Argument(alias = "k", description = "Keyword to search for")
		String keyword;

		@Argument(alias = "l", description = "Maximum number of messages to read")
		Integer reportLimit = 1000;
		@Argument(alias = "o", description = "Timeout in seconds (max 600)")
		Integer timeout = 60;

		@Argument(alias = "d", description = "Where to save output [mongo,hdfs,both]", required = true)
		String output;

		@Argument(alias = "p", description = "HDFS path to save data")
		String outputPath = "/tmp/debugconsumer/";
	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		Logs.Init();
		final Arguments arguments = new Arguments();
		Args.parseOrExit(arguments, args, true);

		// Validate arguments
		if (!TopicSourceMap.getMap().containsKey(arguments.topic)){
			throw new IllegalArgumentException("topic not defined in map");
		}
		if (arguments.reportLimit <= 0){
			throw new IllegalArgumentException("report limit must be positive");
		}
		if (arguments.timeout <= 0){
			throw new IllegalArgumentException("timeout must be positive");
		}
		if (arguments.timeout > 600){
			arguments.timeout = 600;
		}
		if (!arguments.output.equals(OUTPUT_MONGO) && !arguments.output.equals(OUTPUT_HDFS) && !arguments.output.equals(OUTPUT_BOTH)){
			throw new IllegalArgumentException("Output must be one of [mongo,hdfs,both]");
		}

		// if this will not be done before first call to ConfigUtil.GetInstance the default 'production' environment would be used.
		ConfigUtil.SetEnvironment(arguments.env);
		if (StringUtils.isNotBlank(arguments.config)) {
			HDFSFileUtil.SetConfigDir(arguments.config);
		}

		Date query = new Date();
		System.out.println("Query ID: " + Long.toString(query.getTime()));

		final SourceProcessor processor = new SourceProcessor(arguments.topic, arguments.output, Long.toString(query.getTime()));
		processor.maxMessages = arguments.reportLimit;
		SimpleDateFormat spf = new SimpleDateFormat("yyyy'-'MM'-'dd'-'HH'-'mm");
		processor.outputPath = arguments.outputPath + spf.format(query);

		if (StringUtils.isNotBlank(arguments.source)){
			processor.targetSource = arguments.source;
		}
		if (StringUtils.isNotBlank(arguments.keyword)){
			processor.targetKeyword = arguments.keyword;
		}

		listener = new KafkaListener(processor,
				arguments.topic,
				GROUP_ID,
				1);

		listener.DoNotTrackOffsets();
		listener.Start();

		// Initiate timeout
		Timer timer = new Timer(true);
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				log.info("Timeout reached");
				processor.FinishListening();
				System.exit(0);
			}
		}, arguments.timeout * 1000);

		// Attach to HDFS shutdown hook - we don't use the general shutdown hook because there's no guarantee on order,
		// So what happened was that the HDFS shutdown happened first, closed all sockets, then the HDFS Util shutdown happened,
		// And tried to flush file buffers to already closed sockets. This way, we can ensure that the util shutdown happens before the HDFS shutdown,
		// By using a higher priority
		ShutdownHookManager.get().addShutdownHook(new Runnable() {
			@Override
			public void run() {

				listener.Shutdown();

				try {
					HDFSFileUtil.GetInstance().Shutdown();
				} catch (IOException e) {
					log.fatal("Could not connect to HDFS when shutting down", e);
				} catch (URISyntaxException e) {
					log.fatal("HDFS URI invalid!", e);
				}
				ConfigUtil.Shutdown();
				StatusUtil.Shutdown();
			}
		}, FileSystem.SHUTDOWN_HOOK_PRIORITY * 10);
	}

	/**
	 * TrackerProcessor receives old tracker records and writes them out as TSV
	 */
	public static class SourceProcessor extends BaseProcessor {

		private String topic;
		public String targetSource = null;
		public String targetKeyword = null;
		public int maxMessages = 1000;
		public String outputType;
		public String outputPath;
		public String queryid;

		private int readMessages = 0;

		private ArrayList<Document> mongoRecords = new ArrayList<Document>();

		private ObjectMapper mapper = new ObjectMapper(new MessagePackFactory());

		public SourceProcessor(String topic, String output, String query) {
			super("kafka.consumer.sourcetracker." + Logs.getHostName());
			this.topic = topic;
			this.outputType = output;
			this.queryid = query;
		}

		public void FinishListening(){
			if (outputType.equals(OUTPUT_BOTH) || outputType.equals(OUTPUT_MONGO)) {
				MongoClientURI connectionString = new MongoClientURI(MONGO_CONN);
				MongoClient mongoClient = new MongoClient(connectionString);
				MongoDatabase db = mongoClient.getDatabase(MONGO_DB);

				MongoCollection<Document> collection = db.getCollection(MONGO_COLLECTION);
				Document mongoRecord = new Document("qid", queryid)
						.append("desc", String.format("Topic: \"%s\" Source: \"%s\" Keyword: \"%s\"", topic, (targetSource == null ? "All":targetSource), (targetKeyword == null ? "None" : targetKeyword)))
						.append("rows", mongoRecords);

				collection.insertOne(mongoRecord);
			}
		}

		@Override
		public void Process(byte[] data, int threadNum) throws InterruptedException, KafkaMessageException {

			// Serialize message to tracker record object
			JsonNode message;
			try {
				message = mapper.readTree(data);
			} catch (IOException e) {
				log.error("Could not parse read message", e);
				throw new KafkaMessageException(e);
			}

			String messageString = message.toString();

			// Figure out if message fits our parameters
			boolean wanted = true;
			if (StringUtils.isNotBlank(targetSource)){
				JsonNode sourcefield = message.get(TopicSourceMap.getMap().get(topic));
				if ((sourcefield == null) || (!targetSource.equals(sourcefield.textValue()))){
					wanted = false;
				}
			}
			if (StringUtils.isNotBlank(targetKeyword)){
				if (!messageString.contains(targetKeyword)){
					wanted = false;
				}
			}

			// If message wasn't filtered out, report it
			if (wanted) {
				System.out.println(messageString);
				readMessages++;
				if (outputType.equals(OUTPUT_BOTH) || outputType.equals(OUTPUT_HDFS)){
					try {
						HDFSFileUtil.GetInstance().write(outputPath, queryid + ".txt.gz", messageString, false);
					} catch (Exception e) {
						log.error("Could not save message", e);
					}
				}

				if (outputType.equals(OUTPUT_BOTH) || outputType.equals(OUTPUT_MONGO)){
					// Add to mongo records
					mongoRecords.add(Document.parse(messageString));
				}
			}

			// Reported enough messages, stop
			if (readMessages >= maxMessages){
				log.info("Message limit reached");
				FinishListening();

				System.exit(0);
			}
		}
	}
}
