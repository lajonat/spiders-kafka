package com.similargroup.DIConsumer.utils;

public final class Constants {

    public static final String  DIRECT_GRAPHITE_HOST = "graphite-a01.sg.internal";
	public static final int DIRECT_GRAPHITE_PORT = 2003;
	public static final String  GRAPHITE_HOST = "localhost";
    public static final int     GRAPHITE_PORT = 8125;
    
}