package com.similargroup.DIConsumer.utils;

import com.maxmind.db.Reader;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This utility class encapsulates the resolving of Geo location data
 * Created by yotam on 27/12/2015.
 */
public class GeoUtil {

    private static Logger log = Logger.getLogger("kafkalog");
    private static GeoUtil instance;
    private static DatabaseReader dr;

    private static final String GEOLOCATION_DATA_FILE_DEFAULT = "/opt/consumer/GeoIP2-City.mmdb";
    private static final long DEFAULT_GEO_RELOAD_PERIOD = 60 * 60 * 1000;

    private static final String CONFIG_PATH =  "/services/data-ingestion/kafka-consumers/geoutil/";

    private static final String GEO_LOCATION_DATA_FILE_KEY = CONFIG_PATH+"geolocation_file";
    private static final String GEO_RELOAD_PERIOD_KEY =  CONFIG_PATH + "geo-reload-period";

    private Timer geolocationReloaderTimer;

    private GeoUtil() throws IOException {
        loadGeolocationData();
        startTimers();

    }

    private static void loadGeolocationData() throws java.io.IOException {

        ConfigUtil conf = ConfigUtil.getInstance();

        File database = new File(conf.getConfig(GEO_LOCATION_DATA_FILE_KEY, GEOLOCATION_DATA_FILE_DEFAULT));

        DatabaseReader.Builder builder = new DatabaseReader.Builder(database);

        builder.fileMode(Reader.FileMode.MEMORY);

        dr = builder.build();

    }

    private void startTimers() {
        ConfigUtil conf = ConfigUtil.getInstance();

        // Create periodic check timer
        TimerTask tasknew = new GeolocationDataReloadTimer();
        geolocationReloaderTimer = new Timer(true);
        long checkPeriod = conf.getConfigAsLong(GEO_RELOAD_PERIOD_KEY, DEFAULT_GEO_RELOAD_PERIOD);
        geolocationReloaderTimer.schedule(tasknew, checkPeriod, checkPeriod);
    }

    public static String retrievePostalCode(String ipAddress){
        if (ipAddress == null || StringUtils.isBlank(ipAddress)){
            return "";
        }
        try {
            InetAddress inetAddress = InetAddress.getByName(ipAddress);

            CityResponse response = dr.city(inetAddress);

            if (response != null) {
                return response.getPostal().getCode();
            }


        } catch (Exception e) {
        }
        return "";
    }


    /**
     * Shutdowns the Geo util
     */
    public void Shutdown(){
        geolocationReloaderTimer.cancel();
        geolocationReloaderTimer = null;
        try {
            dr.close();
        } catch (java.io.IOException e) {
            log.fatal("Could not close Geo Database Reader", e);
        }


    }

    /**
     * Get the singleton instance of the HDFS File Util
     * @return Instance of file util
     * @throws IOException - Could not create HDFS
     * @throws URISyntaxException - Configuration is invalid
     */
    public synchronized static GeoUtil GetInstance() throws IOException {
        if (instance == null){
            instance = new GeoUtil();
        }
        return instance;
    }

    /**
     * validationTimer holds logic for periodic validation checks on HDFS
     */
    private static class GeolocationDataReloadTimer extends TimerTask {

        public void run(){
            // Only if not valid
            try {
                loadGeolocationData();
                log.info("Reloaded geo location database");
            } catch (IOException e) {
                log.error("Failed while trying to reload geo location database", e);
            }
        }
    }

}
