package com.similargroup.DIConsumer.utils;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import kafka.api.FetchRequestBuilder;
import kafka.javaapi.*;
import kafka.javaapi.consumer.SimpleConsumer;
import kafka.message.MessageAndOffset;
import org.msgpack.jackson.dataformat.MessagePackFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Reads a specific message
 * Created by jonathan on 5/27/2015.
 */
public class SpecificMessageConsumer {

	private static class Arguments {

		@Argument(alias = "t", description = "Topic", required = true)
		String topic;
		@Argument(alias = "p", description = "partition", required = true)
		Integer partition;
		@Argument(alias = "o", description = "Message offset", required = true)
		Long offset;
	}

	public static void main(String[] args) throws IOException {
		final Arguments arguments = new Arguments();
		Args.parseOrExit(arguments, args, true);

		List<String> s = new ArrayList<String>();
		s.add("kafka-a01.sg.internal");
		s.add("kafka-a02.sg.internal");
		s.add("kafka-a03.sg.internal");
		s.add("kafka-a04.sg.internal");
		s.add("kafka-a05.sg.internal");
		PartitionMetadata a = findLeader(s, 9092, arguments.topic, arguments.partition);
		String leadBroker = a.leader().host();
		String clientName = "specific_tracker_client";
		SimpleConsumer consumer = new SimpleConsumer(leadBroker, 9092, 10000, 64 * 1024, clientName);
		kafka.api.FetchRequest req =
				new FetchRequestBuilder()
				.clientId(clientName)
				.addFetch(arguments.topic, arguments.partition, arguments.offset, 100000) // Note: this fetchSize of 100000 might need to be increased if large batches are written to Kafka
				.build();
		FetchResponse fetchResponse = consumer.fetch(req);
		kafka.javaapi.message.ByteBufferMessageSet b = fetchResponse.messageSet("tracker", 2);
		Iterator<MessageAndOffset> it = b.iterator();


		ObjectMapper mapper = new ObjectMapper(new MessagePackFactory());
		if (it.hasNext()) {
			MessageAndOffset c = it.next();
			System.out.println(c.offset());
			byte[] by = new byte[c.message().payload().limit()];
			c.message().payload().get(by);
			JsonNode t = mapper.readTree(by);
			System.out.println(t.toString());
		}
	}

	private static PartitionMetadata findLeader(List<String> a_seedBrokers, int a_port, String a_topic, int a_partition) {
		PartitionMetadata returnMetaData = null;
		loop:
		for (String seed : a_seedBrokers) {
			SimpleConsumer consumer = null;
			try {
				consumer = new SimpleConsumer(seed, a_port, 100000, 64 * 1024, "leaderLookup");
				List<String> topics = Collections.singletonList(a_topic);
				TopicMetadataRequest req = new TopicMetadataRequest(topics);
				TopicMetadataResponse resp = consumer.send(req);

				List<TopicMetadata> metaData = resp.topicsMetadata();
				for (TopicMetadata item : metaData) {
					for (PartitionMetadata part : item.partitionsMetadata()) {
						if (part.partitionId() == a_partition) {
							returnMetaData = part;
							break loop;
						}
					}
				}
			} catch (Exception e) {
				System.out.println("Error communicating with Broker [" + seed + "] to find Leader for [" + a_topic
						+ ", " + a_partition + "] Reason: " + e);
			} finally {
				if (consumer != null) consumer.close();
			}
		}
//		if (returnMetaData != null) {
//			m_replicaBrokers.clear();
//			for (kafka.cluster.Broker replica : returnMetaData.replicas()) {
//				m_replicaBrokers.add(replica.host());
//			}
//		}
		return returnMetaData;
	}
}
