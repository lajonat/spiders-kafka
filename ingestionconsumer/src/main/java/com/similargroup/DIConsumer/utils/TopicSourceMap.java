package com.similargroup.DIConsumer.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Defines the source field per topic - for cross-topic consumers
 * Created by jonathan on 12/15/2015.
 */
public class TopicSourceMap {
	private static Map<String, String> topicMap = null;

	public static Map<String,String> getMap(){
		if (topicMap == null){
			initialize();
		}
		return topicMap;
	}

	private static void initialize() {
		topicMap = new HashMap<String, String>();

		topicMap.put("tracker", "sourceId");
		topicMap.put("test_tracker", "sourceId");
		topicMap.put("advanced_tracker", "sourceId");
		topicMap.put("test_advanced_tracker", "sourceId");
		topicMap.put("mobile_tracker_vpn", "sourceId");
		topicMap.put("test_mobile_tracker_vpn", "sourceId");
		topicMap.put("chrome_sync_history", "s");
		topicMap.put("test_chrome_sync_history", "s");
		topicMap.put("ios", "s");
		topicMap.put("test_ios", "s");
		topicMap.put("mobile", "s");
		topicMap.put("test_mobile", "s");
	}
}
