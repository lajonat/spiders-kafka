package com.similargroup.DIConsumer.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jonathan on 9/3/2015.
 */
public class ConcurrentDateFormatAccess {
	public ConcurrentDateFormatAccess(String format) {
		initialFormat = format;
	}
	private String initialFormat = "";

	private ThreadLocal<DateFormat> df = new ThreadLocal<DateFormat> () {

		@Override
		public DateFormat get() {
			return super.get();
		}

		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat(initialFormat);
		}

		@Override
		public void remove() {
			super.remove();
		}

		@Override
		public void set(DateFormat value) {
			super.set(value);
		}

	};

	public Date convertStringToDate(String dateString) throws ParseException {
		return df.get().parse(dateString);
	}

	public String convertDateToString(Date date) {
		return df.get().format(date);
	}
}
