package com.similargroup.DIConsumer.utils;

import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;
import org.apache.commons.io.output.CountingOutputStream;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.zip.GZIPOutputStream;

/**
 * This utility class encapsulates the writing to HDFS files
 * Created by jonathan on 5/4/2015.
 */
public class HDFSFileUtil {

    private static Logger log = Logger.getLogger("kafkalog");
    private static final StatsDClient statsd = new NonBlockingStatsDClient("kafka.consumer.hdfs", Constants.GRAPHITE_HOST, Constants.GRAPHITE_PORT);

    private static HDFSFileUtil instance;

    private static String configDir = null;

    private static final String DEFAULT_CONF_DIR = "/etc/hadoop/conf/";

    private static final long DEFAULT_FILE_LIMIT = 1024*1024*128;	// How big a file can get before we roll-over and start a new file, in bytes
    private static final long DEFAULT_CHECK_PERIOD = 20000;	// Milliseconds between periodic checks
    private static final long DEFAULT_FILE_TIMEOUT = 60000;	// Milliseconds of inactivity on a specific file before it's closed
    private static final long DEFAULT_VALIDATION_PERIOD = 20000;	// Milliseconds between periodic HDFS validation checks

    private static final String CONFIG_PATH =  "/services/data-ingestion/kafka-consumers/hdfsutil/";

    private static final String FILE_LIMIT_KEY =  CONFIG_PATH + "file-limit";
    private static final String CHECK_PERIOD_KEY =  CONFIG_PATH + "check-period";
    private static final String FILE_INACTIVITY_TIMEOUT_KEY =  CONFIG_PATH + "file-timeout";
    private static final String VALIDATION_PERIOD_KEY =  CONFIG_PATH + "validation-period";
    private static final String CONF_DIR_KEY =  CONFIG_PATH + "default-conf-dir";
    private static final String FILE_ROLL_TIME_KEY = CONFIG_PATH + "file-roll-time";
    public static final String DEFAULT_FILE_ROLL_TIME_VALUE = "HOUR";

    FileSystem hdfs = null;	// HDFS file system root
    HashMap<String, FileWrapper> streams;	// This map holds an open file, by file path - each path can have only one file open per process
    private ReadWriteLock globalLock = new ReentrantReadWriteLock();	// General lock for resetting the HDFS root

    private volatile boolean validState = true; // Whether the HDFS is in valid state - as in usable

    private Timer timer;	// Periodic checks to close inactive files
    private Timer validation;	// Periodic checks to validate HDFS
    private Timer periodicCloser;    // Periodic closer

    private HDFSFileUtil() throws URISyntaxException, IOException {
        reset();

        startTimers();
    }

    private void startTimers() {
        ConfigUtil conf = ConfigUtil.getInstance();

        // Create periodic check timer
        TimerTask tasknew = new CheckTimer(new InactivityFileCloseChecker());
        timer = new Timer(true);
        long checkPeriod = conf.getConfigAsLong(CHECK_PERIOD_KEY, DEFAULT_CHECK_PERIOD);
        timer.schedule(tasknew, checkPeriod, checkPeriod);

        // Create periodic validation timer
        TimerTask validTask = new validationTimer();
        validation = new Timer(true);
        long validationPeriod = conf.getConfigAsLong(VALIDATION_PERIOD_KEY, DEFAULT_VALIDATION_PERIOD);
        validation.schedule(validTask, validationPeriod, validationPeriod);

        // Create periodic file close timer
        TimerTask fileCloserTask = new CheckTimer(new AlwaysCloseChecker());
        periodicCloser = new Timer(true);
        SchedulerConfigValue schedulerConfig = SchedulerConfigValue.valueOf(conf.getConfig(FILE_ROLL_TIME_KEY, DEFAULT_FILE_ROLL_TIME_VALUE));
        periodicCloser.schedule(fileCloserTask, schedulerConfig.getStartDate(), schedulerConfig.getWaitPeriod());
    }

    enum SchedulerConfigValue {

        MINUTE(Calendar.MINUTE, TimeUnit.MINUTES),HOUR(Calendar.HOUR,TimeUnit.HOURS), DAY(Calendar.DAY_OF_MONTH,TimeUnit.DAYS);

        private final int calendarField;

        private final TimeUnit timeUnit;

        private SchedulerConfigValue(int calendarField, TimeUnit timeUnit) {
            this.calendarField = calendarField;
            this.timeUnit = timeUnit;
        }

        Date getStartDate() {
            return DateUtils.round(new Date(), calendarField);
        }

        long getWaitPeriod() {
            return timeUnit.toMillis(1);
        }

    }

    /**
     * Resets the HDFS root - loads configuration and creates HDFS fs
     * @throws URISyntaxException - thrown when loaded HDFS address is invalid
     * @throws IOException - thrown when HDFS can't be created
     */
    private synchronized void reset() throws URISyntaxException, IOException {
        // If already loaded, close active FS
        if (hdfs != null) {
            closeAll();
        }

        // Load configuration
        Configuration config = new Configuration();
        if (configDir != null) {
            config.addResource(new Path(configDir + "core-site.xml"));
            config.addResource(new Path(configDir + "hdfs-site.xml"));
        } else{
            config.addResource(new Path(ConfigUtil.getInstance().getConfig(CONF_DIR_KEY, DEFAULT_CONF_DIR) + "core-site.xml"));
            config.addResource(new Path(ConfigUtil.getInstance().getConfig(CONF_DIR_KEY, DEFAULT_CONF_DIR) + "hdfs-site.xml"));
        }
        config.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        config.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());

        try {
            // When changing the FS root, write-lock, to allow all active threads to finish, and restrict new threads until we have a stable FS
            globalLock.writeLock().lock();
            streams = new HashMap<String, FileWrapper>();
            hdfs = FileSystem.get(config);
            log.info("Opening HDFS");
            statsd.incrementCounter("reset");
        } finally {
            globalLock.writeLock().unlock();
        }
    }

    /**
     * Closes all open files
     */
    private void closeAll(){
        // Go over files, close them - no sync tests here because we're write-locked
        String[] files = streams.keySet().toArray(new String[streams.keySet().size()]);
        for (String fileDir : files) {
            FileWrapper file = streams.get(fileDir);
            if (file != null) {
                try {
                    closeFile(file);
                } catch (IOException e) {
                    log.fatal("Possible corruption - could not close HDFS file " + file.filepath, e);
                    statsd.incrementCounter("corruption");
                    HDFSCorruptionHandler.GetInstance().ValidateFile(file.dir, file.fileName, file.checksum);
                    StatusUtil.getInstance().setStatus(StatusUtil.Status.Corruption, "Got exception while closing HDFS file :" + e.getMessage());
                }
            }
        }

        try {
            hdfs.close();
        } catch (IOException e) {
            log.fatal("Possible corruption - could not close HDFS", e);
            statsd.incrementCounter("corruption");
            // TODO: handle - corruption?
            StatusUtil.getInstance().setStatus(StatusUtil.Status.Corruption, "Got exception while closing HDFS files :" + e.getMessage());
        }
    }

    /**
     * Shutdowns the HDFS file util
     */
    public synchronized void Shutdown(){
        // Mark as not valid - we don't want any more messages coming
        validState = false;

        // Shutdown corruption handler
        HDFSCorruptionHandler.GetInstance().Shutdown();
        try {
            // Write-lock to allow all active threads to finish, and restrict new threads until we're done
            globalLock.writeLock().lock();

            // Stop periodic checks
            timer.cancel();
            timer = null;

            validation.cancel();
            validation = null;

            closeAll();
        } finally {
            globalLock.writeLock().unlock();
        }
    }

    /**
     * Get the singleton instance of the HDFS File Util
     * @return Instance of file util
     * @throws IOException - Could not create HDFS
     * @throws URISyntaxException - Configuration is invalid
     */
    public synchronized static HDFSFileUtil GetInstance() throws IOException, URISyntaxException {
        if (instance == null){
            instance = new HDFSFileUtil();
        }
        return instance;
    }

    /**
     * Sets the config dir to use for HDFS configuration (default /etc/hadoop/conf/)
     * @param path - Path to config dir
     */
    public static void SetConfigDir(String path){
        configDir = path;
    }

    /**
     * Converts a digest byte array to formatted md5 file line
     * @param digest - message digest data for given file
     * @param filename - name of the digest's file
     * @return MD5 formatted line
     */
    static String getMD5Checksum(byte[] digest, String filename) {
        String result = "";

        for (byte aDigest : digest) {
            result += Integer.toString((aDigest & 0xff) + 0x100, 16).substring(1);
        }
        return result + "  " + filename;
    }

    /**
     * Gets a file wrapper for a given file and path. If no current file wrapper exists for path, create one
     * @param path - Directory path of file - without HDFS://...
     * @param filename - Given filename if creating new file wrapper. If one exists, this is ignored
     * @param calcChecksum - Whether to calculate checksum or not for file
     * @return An active file wrapper, that can be written to
     * @throws IOException - Thrown when there was a problem creating file wrapper
     */
    private synchronized FileWrapper getFile(String path, String filename, boolean calcChecksum) throws IOException {
        // Standardize the path that's used as a map key
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }

        // Create new file wrapper if none exist for given path
        if (!streams.containsKey(path)) {
            streams.put(path, new FileWrapper(path + "/" + filename, calcChecksum));
        }

        return streams.get(path);
    }

    /**
     * Writes given data to given file in given path, with checksum
     * @param path - Directory path of file - without HDFS://...
     * @param filename - Wanted filename if creating new file. If one exists and is active, this is ignored
     * @param data - Data to write to file
     */
    public void write(String path, String filename, String data) {
        write(path, filename, data, true);
    }

    /**
     * Writes given data to given file in given path
     * @param path - Directory path of file - without HDFS://...
     * @param filename - Wanted filename if creating new file. If one exists and is active, this is ignored
     * @param data - Data to write to file
     * @param calcChecksum - Whether to calculate checksum or not
     */
    public void write(String path, String filename, String data, boolean calcChecksum) {
        try {
            // Encapsulate only entry point with read lock, so that if there's a change in HDFS root in progress, this will wait until it's done
            globalLock.readLock().lock();

            // Repeat trying to get an active file until successful - synchronization assurance.
            // It is possible that immediately after getting an active file, another thread will close it, and we'll be left with a closed file wrapper
            boolean repeat = true;
            do {
                FileWrapper file = getFile(path, filename, calcChecksum);
                synchronized (file) {
                    if (!file.closed) {
                        // If we get here, we're sure we have an active file wrapper, so no need to repeat
                        // Because we're locked on its instance, no one else should be able to touch it until we're done
                        repeat = false;

                        // Update last written on this file, and write data
                        file.lastWrite = new Date().getTime();
                        file.writer.write(data);

                        // If file exceeds threshold, close it
                        if (file.getBytesWritten() > ConfigUtil.getInstance().getConfigAsLong(FILE_LIMIT_KEY, DEFAULT_FILE_LIMIT)) {
                            closeFile(file);
                        }
                    }
                }
            } while (repeat);
        }catch (IOException ex){
            // Mark as not valid
            validState = false;

            HDFSCorruptionHandler.GetInstance().ValidateFile(path, filename, calcChecksum);
            statsd.incrementCounter("failures");
            log.error("Exception while writing to HDFS - switching to unusable state", ex);
        }finally {
            globalLock.readLock().unlock();
        }
    }

    /**
     * Closes an open file wrapper, and creates a checksum file for it
     * @param file - File wrapper to close
     * @throws IOException - Thrown if there was a problem closing open file, or writing checksum
     */
    private void closeFile(FileWrapper file) throws IOException {
        if (!file.closed) {
            // Remove from open files map
            streams.remove(file.dir);

            // Close (flush data) and mark as closed
            file.writer.close();
            file.closed = true;

            // Create message digest, and write it to a checksum file
            if (file.isChecksum()) {
                byte[] digest = file.digest.digest();
                Path check = new Path(file.filepath + ".md5");
                if (hdfs.exists(check)) {
                    hdfs.delete(check, true);
                }
                OutputStream os = hdfs.create(check);
                BufferedWriter br = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                br.write(getMD5Checksum(digest, file.fileName));
                br.close();
            }
        }
    }

    /**
     * Checks whether the HDFS is usable at this moment
     * @return - whether it's safe to use the HDFS util now
     */
    public boolean isUsable() {
        return validState;
    }

    /**
     * FileWrapper class holds information on an open file in the HDFS, to allow writing to it
     */
    class FileWrapper {
        public String fileName;	// The open file's name
        public String filepath; // Full path of file, including namenode
        public String dir; // The directory of the file, without HDFS://...
        public Writer writer; // Stream writer, to write data to file
        public MessageDigest digest; // The message digest of all the data written so far
        public long lastWrite; // Timestamp of last write operation on this file

        public boolean closed; // Whether this file wrapper was already closed

        private boolean checksum; // Whether or not calculating checksum for file
        private CountingOutputStream count; // Decorator to allow counting number of bytes written to stream

        /**
         * Create and open stream to new file in given path
         * @param path - Full path of file to open, without namenode
         * @param checksum -Whether to calculate checksum
         * @throws IOException - thrown when there was a communication problem with HDFS while opening file
         */
        public FileWrapper(String path, boolean checksum) throws IOException {
            this.checksum = checksum;

            if (checksum) {
                try {
                    digest = MessageDigest.getInstance("MD5");
                } catch (NoSuchAlgorithmException e) {
                    // Shouldn't happen
                    throw new IOException("Cannot create file wrapper, no MD", e);
                }
            }

            // Get path components
            Path file = new Path(path);
            fileName = file.getName();
            filepath = path;
            dir = file.getParent().toString();

            // Create file in given path
            if (hdfs.exists(file)) {
                hdfs.delete(file, true);
            }
            OutputStream os = hdfs.create(file);

            // Create streams : bufferwriter -> gzip -> counting -> digest calculator -> file
            if (checksum) {
                DigestOutputStream dis = new DigestOutputStream(os, digest);
                count = new CountingOutputStream(dis);
            } else{
                count = new CountingOutputStream(os);
            }
            GZIPOutputStream gzip = new GZIPOutputStream(count);
            writer = new BufferedWriter(new OutputStreamWriter(gzip, "UTF-8"));

            // Mark last operation as now, so file won't be closed if there's a check immediately after creating it
            lastWrite = new Date().getTime();

            statsd.incrementCounter("files.created");
        }

        /**
         * Gets whether this file is calculating checksum or not
         * @return Whether this file is calculating checksum
         */
        public boolean isChecksum() {return checksum;}

        /**
         * Gets how many bytes were written to the file so far
         * @return how many bytes were written to the file so far
         */
        public long getBytesWritten(){
            return count.getByteCount();
        }
    }

    interface FileCloseChecker {
        boolean shouldCloseFile(FileWrapper file);
    }

    class InactivityFileCloseChecker implements FileCloseChecker {

        @Override
        public boolean shouldCloseFile(FileWrapper file) {
            if (file.closed) {
                return false;
            }
            // Check if file exceeded inactivity threshold
            long now = new Date().getTime();
            return (now - file.lastWrite > ConfigUtil.getInstance().getConfigAsLong(FILE_INACTIVITY_TIMEOUT_KEY, DEFAULT_FILE_TIMEOUT));
        }
    }

    class AlwaysCloseChecker implements FileCloseChecker {

        @Override
        public boolean shouldCloseFile(FileWrapper file) {
            return !file.closed;
        }
    }


    /**
     * CheckTimer holds logic for periodic checks on open files, and whether to close them
     */
    private class CheckTimer extends TimerTask{

        private final FileCloseChecker fileCloseChecker;

        private CheckTimer(FileCloseChecker fileCloseChecker) {
            this.fileCloseChecker = fileCloseChecker;
        }

        public void run(){
            // No need to run if HDFS is invalid
            if (!validState){
                return;
            }

            try {
                // Encapsulate with read lock, so that if there's a change in HDFS root in progress, this will wait until it's done, and vice-versa
                globalLock.readLock().lock();

                // Go over files, check them
                String[] files = streams.keySet().toArray(new String[streams.keySet().size()]);
                for (String fileDir : files) {
                    FileWrapper file = streams.get(fileDir);
                    // Make sure this file wasn't cleared by another thread
                    if (file != null) {
                        synchronized (file) {
                            // If this file was already closed by the time we got here, just skip it
                            if (fileCloseChecker.shouldCloseFile(file)) {
                                try {
                                    closeFile(file);
                                } catch (IOException e) {
                                    // Mark as not valid
                                    validState = false;
                                    statsd.incrementCounter("failures");
                                    log.fatal("Possible corruption - could not close file " + file.filepath + ". Switching to unusable HDFS state.", e);
                                    statsd.incrementCounter("corruption");
                                    HDFSCorruptionHandler.GetInstance().ValidateFile(file.dir, file.fileName, file.checksum);
                                    StatusUtil.getInstance().setStatus(StatusUtil.Status.Corruption, "Got exception while closing HDFS file :" + e.getMessage());
                                }
                            }
                        }
                    }
                }
            }finally {
                globalLock.readLock().unlock();
            }
        }
    }

    /**
     * validationTimer holds logic for periodic validation checks on HDFS
     */
    private class validationTimer extends TimerTask{
        /**
         * Create a random HDFS path for testing
         * @param basePath - base path in which to create a random directory
         * @return - full path of random dir
         */
        public Path getRandomPath(String basePath) {
            if (!basePath.endsWith("/")) basePath += "/";

            long randLong = Math.abs(UUID.randomUUID().getMostSignificantBits());

            return new Path(basePath.concat(randLong + "/"));
        }

        public void run(){
            // Only if not valid
            if (!validState) {
                try {
                    // Encapsulate with write lock, so that nothing can happen while we're here
                    globalLock.writeLock().lock();
                    log.info("Trying to revalidate HDFS");

                    // Reset HDFS
                    reset();

                    // Get random folder
                    Path folder = getRandomPath("/tmp/");
                    String path = folder.toString();
                    if (path.endsWith("/")) {
                        path = path.substring(0, path.length() - 1);
                    }

                    // Write test file
                    write(path, "test.dat", "test");
                    FileWrapper file = streams.get(path);
                    if (file != null){
                        // Finalize test file
                        closeFile(file);

                        // Make sure that file was written, along with checksum
                        Path filepath = new Path(file.filepath);
                        Path md5path = new Path(file.filepath + ".md5");

                        if ((hdfs.exists(filepath)) && (hdfs.exists(md5path))){
                            // Delete test folder
                            hdfs.delete(folder, true);

                            // If we got here with no exceptions, it means that the HDFS is usable - we wrote a file and deleted it
                            validState = true;

                            log.info("HDFS Success - switching back to usable state.");
                        }
                    }
                } catch (IOException e) {
                    statsd.incrementCounter("failures");
                    log.warn("HDFS still unusable.", e);
                } catch (URISyntaxException e) {
                    log.fatal("HDFS URI invalid!", e);
                } finally {
                    globalLock.writeLock().unlock();
                }
            }
        }
    }
}
