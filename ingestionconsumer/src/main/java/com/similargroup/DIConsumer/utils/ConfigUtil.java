package com.similargroup.DIConsumer.utils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.util.Base64;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * ConfigUtil maintains Consul configuration values
 * Created by jonathan on 5/19/2015.
 */
public class ConfigUtil {
	private static Logger log = Logger.getLogger("kafkalog");

	private static String runningEnvironment = "production";
	private static final String consulServer = "http://consul.service.%s:8500/v1/kv";

	private static ConfigUtil instance;	// Singleton instance for configuration

	private HttpClient client;
	private ExecutorService es; // For background watcher threads
	private static HashMap<String, String> configValues = new HashMap<String, String>();	// Cache for configuration values
	private static HashMap<String, HttpGet> watchers = new HashMap<String, HttpGet>();	// Each config key that we get has a watcher for changes

	private ConfigUtil(){
		client = HttpClientBuilder.create().build();

		// Initialize threads as daemons, so that won't keep the process running
		es = Executors.newCachedThreadPool(new ThreadFactory() {
			public Thread newThread(Runnable r) {
				Thread t = Executors.defaultThreadFactory().newThread(r);
				t.setDaemon(true);
				return t;
			}
		});
	}

	public static void SetEnvironment(String env){
		runningEnvironment = env;
	}

	/**
	 * Get singleton instance of config util
	 * @return instance of config utility
	 */
	public static ConfigUtil getInstance(){
		if (instance == null){
			instance = new ConfigUtil();
		}

		return instance;
	}

	private String getServer(){
		return String.format(consulServer, runningEnvironment);
	}

	/**
	 * Get the config value for given key. If there's a problem, return the default value supplied.
	 * Starts a watcher on the key if successful, so that will reload automatically when changed
	 * @param key - The config key to get. Should start with /v1/...
	 * @param defaultValue - The value to return if there's a problem, like key not found
	 * @return - The value for the key, either loaded, cached or default
	 */
	public String getConfig(String key, String defaultValue) {
		try {
			// No value cached yet
			if (!configValues.containsKey(key)) {
				// Get value from Consul and cache it
				HttpGet get = new HttpGet(getServer() + key);
				InputStream is = client.execute(get).getEntity().getContent();
				String valueResp = IOUtils.toString(is);
				is.close();
				JSONObject valueNode = new JSONObject(StringUtils.strip(valueResp, "[]"));

				// Value is base64 encoded
				configValues.put(key, new String(Base64.decodeBase64(valueNode.getString("Value"))));

				long mod = valueNode.getLong("ModifyIndex");

				// Start watching key for changes
				watch(key, mod);
			}

			return configValues.get(key);
		} catch (Exception e){
			log.warn("Could not get config key " + key, e);
			return defaultValue;
		}
	}

	/**
	 * Get the config value for given key. If there's a problem, return null.
	 * Starts a watcher on the key if successful, so that will reload automatically when changed
	 * @param key - The config key to get. Should start with /v1/...
	 * @return - The value for the key, if loaded or cached. Null if there was a problem
	 */
	public String getConfig(String key){
		return getConfig(key, null);
	}

	/**
	 * Get the config value for given key as an integer. If there's a problem, return the default value supplied.
	 * Starts a watcher on the key if successful, so that will reload automatically when changed
	 * @param key - The config key to get. Should start with /v1/...
	 * @param defaultValue - The value to return if there's a problem, like key not found
	 * @return - The int value for the key, either loaded, cached or default
	 */
	public int getConfigAsInt(String key, int defaultValue) {
		int result = defaultValue;

		String val = getConfig(key);
		if (val != null) {
			try {
				result = Integer.parseInt(val);
			} catch (NumberFormatException e){
				log.warn("Could not parse config key " + key + " as int", e);
			}
		}

		return result;
	}

	/**
	 * Get the config value for given key as a long. If there's a problem, return the default value supplied.
	 * Starts a watcher on the key if successful, so that will reload automatically when changed
	 * @param key - The config key to get. Should start with /v1/...
	 * @param defaultValue - The value to return if there's a problem, like key not found
	 * @return - The long value for the key, either loaded, cached or default
	 */
	public long getConfigAsLong(String key, long defaultValue) {
		long result = defaultValue;

		String val = getConfig(key);
		if (val != null) {
			try {
				result = Long.parseLong(val);
			} catch (NumberFormatException e){
				log.warn("Could not parse config key " + key + " as long", e);
			}
		}

		return result;
	}

	/**
	 * Get the config value for given key as an boolean. If there's a problem, return the default value supplied.
	 * Starts a watcher on the key if successful, so that will reload automatically when changed
	 * @param key - The config key to get. Should start with /v1/...
	 * @param defaultValue - The value to return if there's a problem, like key not found
	 * @return - The bool value for the key, either loaded, cached or default
	 */
	public boolean getConfigAsBool(String key, boolean defaultValue) {
		boolean result = defaultValue;

		String val = getConfig(key);
		if (val != null) {
			try {
				result = Boolean.parseBoolean(val);
			} catch (NumberFormatException e){
				log.warn("Could not parse config key " + key + " as bool", e);
			}
		}

		return result;
	}

	/**
	 * Shut down the config util - should be called when process is terminating
	 */
	public static void Shutdown(){
		if (instance != null) {
			// Cancel all watchers
			for (HttpGet watcher : watchers.values()) {
				watcher.abort();
			}

			instance.es.shutdownNow();
			instance.es = null;

			instance.client = null;
			instance = null;
		}
	}

	/**
	 * Starts watch the given key for changes
	 * @param key - the key to listen for changes - should start /v1...
	 * @param modifyindex - given when getting the value - will return when this field will be higher than the given value
	 */
	private void watch(final String key, final long modifyindex) {

		final HttpGet getKey = new HttpGet(String.format("%s%s?index=%s&wait=5m", getServer(), key, modifyindex));
		watchers.put(key, getKey);

		// Run the watcher in a background thread
		es.execute(new Runnable() {
			@Override
			public void run() {
				try {
					InputStream is = client.execute(getKey).getEntity().getContent();
					String valueResp = IOUtils.toString(is);
					is.close();
					JSONObject valueNode = new JSONObject(StringUtils.strip(valueResp, "[]"));

					// Value is base64 encoded
					configValues.put(key, new String(Base64.decodeBase64(valueNode.getString("Value"))));

					// Restart watcher - it is only good for one change
					watch(key, valueNode.getLong("ModifyIndex"));
				}catch (Exception e){
					log.warn("Exception caught while reloading config key " + key, e);
				}
			}
		});
	}

	/**
	 * Sets a config key to have the given value
	 * @param key - The config key to set, should start with /v1/...
	 * @param value - The value to place in the config key
	 */
	public void setConfig(String key, String value){
		try {
			HttpPut putreq = new HttpPut(getServer() + key);
			putreq.setEntity(new StringEntity(value));
			client.execute(putreq);
		} catch (Exception e) {
			log.warn("Could not set config key " + key + " with value " + value, e);
		}
	}
}
