package com.similargroup.DIConsumer.utils;

import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * This utility class handles files that are suspected as corrupted
 * Created by jonathan on 8/25/2015.
 */
public class HDFSCorruptionHandler {

	private static Logger log = Logger.getLogger("kafkalog");
	private static final StatsDClient statsd = new NonBlockingStatsDClient("kafka.consumer.hdfs", Constants.GRAPHITE_HOST, Constants.GRAPHITE_PORT);

	private static HDFSCorruptionHandler instance;

	private static final long DEFAULT_VALIDATION_PERIOD = 60000;	// Milliseconds between periodic HDFS validation checks

	private List<CheckedFile> filesToCheck = new Vector<CheckedFile>();	// Files that are suspected as corrupted - to be validated
	private List<CheckedFile> filesToSave = new Vector<CheckedFile>();	// Files that were corrupted and a chunk can be saved - to be saved

	private Timer timer;	// Periodic checks to close inactive files

	private HDFSCorruptionHandler() {
		startTimers();
	}

	private void startTimers() {
		// Create periodic check timer
		TimerTask tasknew = new checkTimer();
		timer = new Timer(true);
		long checkPeriod = DEFAULT_VALIDATION_PERIOD;
		timer.schedule(tasknew, checkPeriod, checkPeriod);
	}

	/**
	 * Shutdowns the HDFS corruption util
	 */
	public synchronized void Shutdown(){
		// Stop periodic checks
		timer.cancel();
		timer = null;
	}

	/**
	 * Get the singleton instance of the HDFS corruption handler Util
	 * @return Instance of corruption handler util
	 */
	public synchronized static HDFSCorruptionHandler GetInstance() {
		if (instance == null){
			instance = new HDFSCorruptionHandler();
		}
		return instance;
	}

	/**
	 * Submit a file that is suspected of being corrupted to the validation queue
	 * @param path - folder path of the file, without trailing /
	 * @param filename - the file name to check
	 * @param checksum - whether this file should have a checksum or not
	 */
	public void ValidateFile(String path, String filename, boolean checksum){
		filesToCheck.add(new CheckedFile(path, filename, checksum));
		statsd.count("files.validating", 1);
	}

	/**
	 * Go over the files queued for validation, try to read each one. If there's a problem with one, slot it for resaving. If it's OK, remove from the queue
	 */
	private void checkFiles() {

		// Go over all files that are still to be checked
		for (int fileIndex = 0; fileIndex < filesToCheck.size(); fileIndex++) {
			CheckedFile currentFile = filesToCheck.get(fileIndex);
			BufferedReader bf = null;
			BufferedWriter bw = null;
			boolean encounteredError = false;
			boolean hasData = false;
			try {
				// Make sure file was already closed by HDFS file util
				if (HDFSFileUtil.GetInstance().streams.containsKey(currentFile.dir) &&
						HDFSFileUtil.GetInstance().streams.get(currentFile.dir).fileName.equals(currentFile.fileName)){

					// If it's not yet closed, wait until it is. For now, continue to the next file
					continue;
				}

				Path p = new Path(currentFile.filepath);

				// If file doesn't exist, remove it from the list and continue to the next file
				if (!HDFSFileUtil.GetInstance().hdfs.exists(p)){
					fileIndex = removeFromList(filesToCheck, fileIndex);
					continue;
				}

				// If file is empty, delete it and continue to the next file
				if (HDFSFileUtil.GetInstance().hdfs.getContentSummary(p).getLength() == 0){
					HDFSFileUtil.GetInstance().hdfs.delete(p, false);
					fileIndex = removeFromList(filesToCheck, fileIndex);
					continue;
				}

				// Read file from HDFS
				FSDataInputStream read = HDFSFileUtil.GetInstance().hdfs.open(p);
				GZIPInputStream readg = new GZIPInputStream(read);
				InputStreamReader reader = new InputStreamReader(readg);
				bf = new BufferedReader(reader);

				// Write to local buffer file
				File fout = new File(currentFile.fileName);
				if (fout.exists()){
					// If a local file by the same name already exists, delete it
					if (!fout.delete()) {
						log.fatal("Cannot create local file!");
						continue;
					}
				}
				FileOutputStream fos = new FileOutputStream(fout);
				bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"));

				// Read each line of data from HDFS, and write it to the local file
				String line;
				try {
					while ((line = bf.readLine()) != null) {
						bw.write(line);
						bw.newLine();

						// If wrote at least one line, we should save the restored file back to HDFS
						hasData = true;
					}
				} catch (EOFException e) {
					// If we got here, it means that the file is corrupted, so we need to fix it
					encounteredError = true;
				}

				// Remove from files to check list - either way we're done reading it
				fileIndex = removeFromList(filesToCheck, fileIndex);

				// So we read everything there is to read from file: If there was an error, and it had data, we need to resave it - add the file to the list of files to save
				if (encounteredError && hasData) {
					filesToSave.add(currentFile);
				} else {
					// If encountered an error but could not get any data - it means that the first chunk is corrupted.
					// So we delete the file
					if (encounteredError){
						bf.close();
						HDFSFileUtil.GetInstance().hdfs.delete(p, false);
					}

					// Either File seems OK or is empty - remove local file because it's not needed anyway
					bw.close();
					if (!fout.delete()) {
						log.warn("Could not delete local file");
					}
				}
			} catch (Exception e) {
				// If there was an unexpected error, like cannot open HDFS at all, continue to the next file
				// It will still be in the queue for the next pass
				log.error("Could not validate file!", e);
				statsd.count("files.failedrecovery", 1);
			} finally {
				// Make sure all streams are closed - no matter if we had an error or not
				closeStreams(bf, bw);
			}
		}
	}

	/**
	 * Go over the files that were recovered and resave them instead of the corrupted files
	 */
	private void saveFiles() {
		// Go over files
		for (int fileIndex = 0; fileIndex < filesToSave.size(); fileIndex++) {
			CheckedFile currentFile = filesToSave.get(fileIndex);
			BufferedReader bf = null;
			BufferedWriter bw = null;
			boolean encounteredError = false;

			// Read local recovered file - if there's a problem reading from the local file, we have to rerecover the file from HDFS
			File fin = new File(currentFile.fileName);
			try {
				FileInputStream fos = new FileInputStream(fin);
				bf = new BufferedReader(new InputStreamReader(fos));
			} catch (Exception ex){
				log.warn("Could not read local file", ex);
				statsd.count("files.failedrecovery", 1);

				// Couldn't read local file - retry reading from HDFS
				ValidateFile(currentFile.dir, currentFile.fileName, currentFile.checksum);

				// Remove from files to save list
				fileIndex = removeFromList(filesToSave, fileIndex);
				continue;
			}

			try{
				Path p = new Path(currentFile.filepath);

				// If the file already exists at destination, delete it
				if (HDFSFileUtil.GetInstance().hdfs.exists(p)){
					HDFSFileUtil.GetInstance().hdfs.delete(p, false);
				}

				// Open a stream to the wanted file destination
				OutputStream os = HDFSFileUtil.GetInstance().hdfs.create(p);
				GZIPOutputStream gzip;

				// If wanted a checksum, create a digest stream between the output and the gzip stream. Otherwise connect them directly
				MessageDigest digest = null;
				if (currentFile.checksum) {
					try {
						digest = MessageDigest.getInstance("MD5");
						DigestOutputStream dis = new DigestOutputStream(os, digest);
						gzip = new GZIPOutputStream(dis);
					} catch (NoSuchAlgorithmException e) {
						// Shouldn't happen
						throw new IOException("Cannot create file wrapper, no MD", e);
					}
				}else{
					gzip = new GZIPOutputStream(os);
				}
				bw = new BufferedWriter(new OutputStreamWriter(gzip, "UTF-8"));

				try {
					String line;
					while ((line = bf.readLine()) != null) {

						// Write data to HDFS file. If we have an error here, it means the HDFS is still not stable.
						// So we stop, and we will retry in the next pass.
						try {
							bw.write(line);
							bw.newLine();
						} catch (Exception e) {
							// Stop this file, don't save it
							log.warn("Still can't write file!", e);
							encounteredError = true;
							break;
						}
					}
				}catch (Exception ex){
					// This exception should only occur when there's a problem reading from the local file
					// Because we've caught the exception from the HDFS already
					log.warn("Could not read local file", ex);
					statsd.count("files.failedrecovery", 1);

					// Couldn't read local file - retry reading from HDFS (probably won't work because we've already deleted it)
					ValidateFile(currentFile.dir, currentFile.fileName, currentFile.checksum);

					// Remove from files to save list
					fileIndex = removeFromList(filesToSave, fileIndex);
					continue;
				}

				// Try to close HDFS file
				bw.close();

				// If wanted a checksum, create it now
				if (currentFile.checksum && digest != null){
					byte[] digestData = digest.digest();
					Path check = new Path(currentFile.filepath + ".md5");
					if (HDFSFileUtil.GetInstance().hdfs.exists(check)) {
						HDFSFileUtil.GetInstance().hdfs.delete(check, false);
					}
					OutputStream osdig = HDFSFileUtil.GetInstance().hdfs.create(check);
					BufferedWriter br = new BufferedWriter(new OutputStreamWriter(osdig, "UTF-8"));
					br.write(HDFSFileUtil.getMD5Checksum(digestData, currentFile.fileName));
					br.close();
				}

				// If we got here, we successfully recovered the file
				statsd.count("files.recovered", 1);

				// Remove from files to save list - if no errors encountered until now. If there was an error, keep on the list, we'll retry it the next pass
				if (!encounteredError) {
					fileIndex = removeFromList(filesToSave, fileIndex);
				}

				// Close and delete the local file, we don't need it anymore
				bf.close();
				if (!fin.delete()){
					log.warn("Could not delete local file");
				}

			} catch (Exception e) {
				// If there was an unexpected error, like cannot open HDFS at all, continue to the next file
				// It will still be in the queue for the next pass
				log.error("Could not resave file!", e);
				statsd.count("files.failedrecovery", 1);
			} finally{
				// Make sure all streams are closed - no matter if we had an error or not
				closeStreams(bf, bw);
			}
		}
	}

	/**
	 * Remove an element from the given list, update index to reflect this
	 * @param list - this list to remove from
	 * @param fileIndex - the index of the element to remove
	 * @return - the new index to use
	 */
	private int removeFromList(List<CheckedFile> list, int fileIndex) {
		list.remove(fileIndex);
		fileIndex--;
		return fileIndex;
	}

	/**
	 * Close read & write streams
	 * @param bf read stream
	 * @param bw write stream
	 */
	private void closeStreams(BufferedReader bf, BufferedWriter bw) {
		if (bf != null) {
			try {
				bf.close();
			} catch (IOException e1) {
				log.error("Could not close input file!", e1);
			}
		}
		if (bw != null) {
			try {
				bw.close();
			} catch (IOException e1) {
				log.error("Could not close output file!", e1);
			}
		}
	}

	/**
	 * CheckedFile class holds information on a suspected corrupted file in the HDFS
	 */
	private class CheckedFile {
		public String fileName;	// The open file's name
		public String filepath; // Full path of file, including namenode
		public String dir; // The directory of the file, without HDFS://...

		public boolean checksum; // Whether file has a checksum

		public CheckedFile(String path, String filename, boolean checksum) {
			// Get path components
			Path file = new Path(path + "/" + filename);
			fileName = file.getName();
			dir = path;
			filepath = file.toString();

			this.checksum = checksum;
		}
	}

	/**
	 * checkTimer holds logic for periodic checks on open files, and whether to close them
	 */
	private class checkTimer extends TimerTask{
		public void run(){
			// No need to run if HDFS is invalid
			try {
				if (HDFSFileUtil.GetInstance().isUsable()){
					checkFiles();
					saveFiles();
				}
			} catch (IOException e) {
				log.fatal("Could not connect to HDFS!", e);
			} catch (URISyntaxException e) {
				log.fatal("HDFS URI invalid!", e);
			}
		}
	}
}
