package com.similargroup.DIConsumer.utils;

import org.apache.log4j.Logger;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.codec.http.*;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.*;
import static org.jboss.netty.handler.codec.http.HttpHeaders.is100ContinueExpected;
import static org.jboss.netty.handler.codec.http.HttpHeaders.isKeepAlive;
import static org.jboss.netty.handler.codec.http.HttpResponseStatus.CONTINUE;
import static org.jboss.netty.handler.codec.http.HttpResponseStatus.OK;
import static org.jboss.netty.handler.codec.http.HttpVersion.HTTP_1_1;

/**
 * StatusUtil exposes the consumer's status to monitoring
 * Created by jonathan on 6/16/2015.
 */
public class StatusUtil extends SimpleChannelUpstreamHandler {
	private static Logger log = Logger.getLogger("kafkalog");

	private static StatusUtil instance;	// Singleton instance for configuration

	public static final int DEFAULT_PORT = 12345;
	private static int port = DEFAULT_PORT;

	private ServerBootstrap server;

	private static final String template = "{\"Status\":\"%s\",\"Reason\":\"%s\", \"Messages\":%s}";
	private byte[] currentStatus;
	private Status currentState;
	private volatile long messages = 0l;

	public static void setPort(int wantedPort){
		port = wantedPort;
	}
	private StatusUtil(){
		this.setStatus(Status.Initializing, "Init");

		// Start server
		server = new ServerBootstrap(
				new NioServerSocketChannelFactory(
						Executors.newSingleThreadExecutor(),
						Executors.newSingleThreadExecutor()
				)
		);

		final StatusUtil handler = this;
		server.setPipelineFactory(new ChannelPipelineFactory() {
			@Override
			public ChannelPipeline getPipeline() throws Exception {
				ChannelPipeline pipeline = new DefaultChannelPipeline();
				pipeline.addLast("decoder", new HttpRequestDecoder());
				pipeline.addLast("encoder", new HttpResponseEncoder());
				pipeline.addLast("deflater", new HttpContentCompressor());
				pipeline.addLast("handler", handler);
				return pipeline;
			}
		});

		server.bind(new InetSocketAddress(port));
	}

	/**
	 * Get singleton instance of status util
	 * @return instance of status utility
	 */
	public static StatusUtil getInstance(){
		if (instance == null){
			instance = new StatusUtil();
		}

		return instance;
	}

	/**
	 * Shut down the status util - should be called when process is terminating
	 */
	public static void Shutdown(){
		if (instance != null) {
			instance.server.releaseExternalResources();
		}
	}

	/**
	 * Updates the current status of the consumer
	 * @param state - The state of the consumer process
	 * @param reason - Why the consumer is in this state
	 */
	public void setStatus(Status state, String reason){
		if (!state.equals(currentState)) {
			String newStatus = String.format(template, state.stringValue, reason, messages);
			currentStatus = newStatus.getBytes();
			currentState = state;
		}
	}

	/**
	 * Increment message counter for this process
	 */
	public synchronized void incrementMessages(){
		messages++;
	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
		Object msg = e.getMessage();
		Channel ch = e.getChannel();
		if (msg instanceof HttpRequest) {
			HttpRequest req = (HttpRequest) msg;

			if (is100ContinueExpected(req)) {
				Channels.write(ctx, Channels.future(ch), new DefaultHttpResponse(HTTP_1_1, CONTINUE));
			}

			boolean keepAlive = isKeepAlive(req);
			HttpResponse response = new DefaultHttpResponse(HTTP_1_1, OK);

			response.setContent(ChannelBuffers.wrappedBuffer(currentStatus));
			response.setHeader(CONTENT_TYPE, "application/json");
			response.setHeader(CONTENT_LENGTH, response.getContent().readableBytes());

			if (!keepAlive) {
				ChannelFuture f = Channels.future(ch);
				f.addListener(ChannelFutureListener.CLOSE);
				Channels.write(ctx, f, response);
			} else {
				response.setHeader(CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
				Channels.write(ctx, Channels.future(ch), response);
			}
		}
	}

	public enum Status{
		Initializing("Initializing"),
		Down("Down"),
		Warning("Warning"),
		Up("Up"),
		Corruption("CORRUPTION"),
		Shutdown("Shutdown");

		private final String stringValue;
		private Status(final String s) { stringValue = s; }
		public String toString() { return stringValue; }
	}
}
