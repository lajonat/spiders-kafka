package com.similargroup.DIConsumer.utils;

import com.similargroup.DIConsumer.base.IKafkaProcessor;
import com.similargroup.DIConsumer.base.KafkaMessageException;
import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;
import kafka.consumer.*;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.MessageAndMetadata;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Listener service, that reads messages from kafka, and passes them to a defined processor
 * Created by jonathan on 4/30/2015.
 */
public class KafkaListener {

	private static Logger log = Logger.getLogger("kafkalog");
	private static final StatsDClient statsd = new NonBlockingStatsDClient("kafka.consumer.listener", Constants.GRAPHITE_HOST, Constants.GRAPHITE_PORT);

	private static final String CONFIG_PATH = "/services/data-ingestion/kafka-consumers/kafka-listener/";

	private int threadNum;	// How many threads to create for receiving messages
	private String topic;	// Which topic to listen to
	private String groupID;	// ID of the consumer group - should be identical on all consumers that process same data
	private ExecutorService executor;	// Thread pool
	private ConsumerConnector consumer;	// Connector to kafka
	private List<ConsumerRunnable> consumerThreads;	// List of running listener threads
	private IKafkaProcessor processor;	// Processor for received messages
	private boolean started = false; // Whether listening already

	private boolean trackOffsets = true; // Whether to track offset persistently

	private static Random rnd = new Random();
	/**
	 * Create a new kafka listener
	 * @param processor - The processor logic to call for each message
	 * @param topic - Name of kafka topic to get messages from
	 * @param groupid - ID of consumer group
	 * @param threads - Number of threads to create for listening to kafka
	 */
	public KafkaListener(IKafkaProcessor processor, String topic, String groupid, int threads){
		if (processor == null){
			throw new NullArgumentException("processor");
		}
		if (StringUtils.isBlank(topic)){
			throw new IllegalArgumentException("topic");
		}
		if (StringUtils.isBlank(groupid)){
			throw new IllegalArgumentException("groupid");
		}
		if (threads < 1){
			threads = 1;
		}
		this.processor = processor;
		this.topic = topic;
		this.groupID = groupid;
		this.threadNum = threads;
	}

	public void DoNotTrackOffsets(){
		this.trackOffsets = false;
	}

	/**
	 * Create kafka consumer config, merging predetermined properties with instance variables
	 */
	private ConsumerConfig getConfig(){
		ConfigUtil conf = ConfigUtil.getInstance();

		Properties props = new Properties();
		props.put("zookeeper.connect", conf.getConfig(CONFIG_PATH + "zookeeper.connect"));
		props.put("group.id", groupID);
		props.put("zookeeper.session.timeout.ms", conf.getConfig(CONFIG_PATH + "zookeeper.session.timeout.ms"));
		props.put("zookeeper.sync.time.ms", conf.getConfig(CONFIG_PATH + "zookeeper.sync.time.ms"));
		props.put("consumer.id", rnd.nextLong() + Logs.getHostName());
		props.put("consumer.timeout.ms", conf.getConfig(CONFIG_PATH + "consumer.timeout.ms")); // This property makes sure we don't block when there are no more messages to read, but we get a timeout
		if (this.trackOffsets) {
			// We want to track offsets - auto commit progress, and try to read old messages
			props.put("auto.commit.interval.ms", conf.getConfig(CONFIG_PATH + "auto.commit.interval.ms"));
			props.put("auto.offset.reset", "smallest");
		} else{
			// We do not want to track offsets - always start from the latest position
			props.put("auto.commit.enable", "false");
			props.put("auto.offset.reset", "largest");
		}
		return new ConsumerConfig(props);
	}

	/**
	 * Start receiving messages from kafka - creates threads, so returns immediately
	 */
	public void Start(){
		if (!started) {
			StatusUtil.getInstance().setStatus(StatusUtil.Status.Initializing, "Initializing");

			// Create thread pool with wanted number of threads
			executor = Executors.newFixedThreadPool(threadNum);

			consumer = Consumer.createJavaConsumerConnector(getConfig());

			// Create connection to kafka topic - we get a stream for each thread to use
			Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
			topicCountMap.put(topic, threadNum);
			Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCountMap);
			List<KafkaStream<byte[], byte[]>> streams = consumerMap.get(topic);

			// Create and run consumer threads
			consumerThreads = new ArrayList<ConsumerRunnable>();
			for (int i = 0; i < threadNum; i++) {
				KafkaStream<byte[], byte[]> st = streams.get(i);
				ConsumerRunnable th = new ConsumerRunnable(processor, st, i);
				consumerThreads.add(th);
				executor.submit(th);
			}

			log.info("Started kafka listening on " + topic + " with " + threadNum + " threads");
			started = true;
			StatusUtil.getInstance().setStatus(StatusUtil.Status.Up, "Listening on kafka");
		}
	}

	/**
	 * Shutdown a running listener - will block until all threads finish
	 */
	public void Shutdown(){
		if (started) {
			log.info("Shutting down kafka listener");
			StatusUtil.getInstance().setStatus(StatusUtil.Status.Shutdown, "Shutting down...");

			if (consumerThreads != null) {
				for (ConsumerRunnable th : consumerThreads) {
					th.Stop();
				}
			}

			// Shut down thread pool, wait for all threads to finish
			executor.shutdownNow();
			try {
				executor.awaitTermination(ConfigUtil.getInstance().getConfigAsInt(CONFIG_PATH + "shutdown-timeout", 20), TimeUnit.SECONDS);
			} catch (InterruptedException ignored) {}

			if (consumer != null) {
				consumer.shutdown();
			}

			log.info("Shut down kafka listener");
			started = false;
		}
	}

	/**
	 * Consumer thread for reading and processing messages from kafka
	 */
	private static class ConsumerRunnable implements Runnable {
		private ConsumerIterator<byte[], byte[]> iterator;
		private IKafkaProcessor processor;
		private int threadNum;
		private volatile boolean stop = false;

		public ConsumerRunnable(IKafkaProcessor processor, KafkaStream<byte[], byte[]> stream, int threadNum) {
			this.processor = processor;
			this.iterator = stream.iterator();
			this.threadNum = threadNum;
		}

		/**
		 * Stop processing after the current message
		 */
		public void Stop(){
			stop = true;
		}

		/**
		 * Receive messages from Kafka in a loop, pass to processor, until stopped
		 */
		public void run() {
			MessageAndMetadata<byte[], byte[]> message = null;
			while (!stop) {
				try {
					// Check if stream has message waiting
					if (hasNext()) {
						message = iterator.next();
						StatusUtil.getInstance().incrementMessages();

						// Sample rate of 1%
						if (rnd.nextDouble() <= 0.01) {
							statsd.count("messages.read", 1, 0.01);
						}
						processor.Process(message.message(), threadNum);
						StatusUtil.getInstance().setStatus(StatusUtil.Status.Up, "Listening on kafka");
					}
				}
				catch (InterruptedException e){
					// If thread was interrupted, it means we're stopping the client
					stop = true;
				}
				catch (KafkaMessageException ex){
					String messageData;
					try {
						messageData = message.topic() + ":" + message.partition() + ":" + message.offset();
					}catch (Exception e){
						messageData = "Could not read message data";
					}

					statsd.incrementCounter("messages.failed");
					log.error("Exception on processing message from kafka: " + messageData, ex);
					StatusUtil.getInstance().setStatus(StatusUtil.Status.Warning, "Error processing message:" + ex.getMessage());
				}
				catch (Exception ex){
					statsd.incrementCounter("errors");
					log.error("Exception on reading message from kafka.", ex);
					StatusUtil.getInstance().setStatus(StatusUtil.Status.Warning, "Exception reading from kafka:" + ex.getMessage());
				}
				// This may happen if we exceed the ulimit - in that case, shutdown gracefully
				catch (OutOfMemoryError e) {
					log.fatal("ulimit reached?", e);
					System.exit(0);
				}
			}
		}

		/**
		 * Check if there are messages waiting in Kafka,
		 * waiting until timeout (10ms by default) for messages to arrive.
		 * and catching the timeout exception to return a boolean
		 */
		private boolean hasNext() {
			try {
				iterator.hasNext();
				return true;
			} catch (ConsumerTimeoutException e) {
				return false;
			}
		}
	}
}
