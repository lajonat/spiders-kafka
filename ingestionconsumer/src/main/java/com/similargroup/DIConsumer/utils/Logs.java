package com.similargroup.DIConsumer.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.MDC;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Initializes logging at start
 * Created by jonathan on 5/18/2015.
 */
public class Logs {
	public static void Init(){
		RuntimeMXBean rt = ManagementFactory.getRuntimeMXBean();
		String pid = rt.getName();
		MDC.put("PID", pid);
	}

	public static String getHostName(){
		try {
			return StringUtils.replaceChars(InetAddress.getLocalHost().getHostName(), '.', '_');
		} catch (UnknownHostException e) {
			return "Unknown";
		}
	}
}
