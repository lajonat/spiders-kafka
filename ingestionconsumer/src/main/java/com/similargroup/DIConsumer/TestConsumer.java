package com.similargroup.DIConsumer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.similargroup.DIConsumer.base.IKafkaProcessor;
import com.similargroup.DIConsumer.utils.ConfigUtil;
import com.similargroup.DIConsumer.utils.KafkaListener;
import com.similargroup.DIConsumer.utils.StatusUtil;
import org.msgpack.jackson.dataformat.MessagePackFactory;

import java.io.IOException;

/**
 * Reads messages from test topic, and prints to console
 * Created by jonathan on 4/30/2015.
 */
public class TestConsumer {
	public static void main(String[] args) {
		TestProcessor processor = new TestProcessor();

		KafkaListener listener = new KafkaListener(processor, "test_test", "testconsu", 1);

		listener.Start();

		while (!processor.stop){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		listener.Shutdown();
		ConfigUtil.Shutdown();
		StatusUtil.Shutdown();
	}

	public static class TestProcessor implements IKafkaProcessor {

		ObjectMapper mapper = new ObjectMapper(new MessagePackFactory());

		int counter = 0;

		public boolean stop = false;

		@Override
		public void Process(byte[] data, int threadNum) {
			try {
				System.out.println("Reading:");
				JsonNode read = mapper.readTree(data);
				System.out.println(read.toString());
				System.out.println("---------------------");
			} catch (IOException e) {
				e.printStackTrace();
			}
			counter++;

			if (counter % 10 == 0){
				String input = System.console().readLine();
				if (input.equals("q")){
					stop = true;
				}
			}
		}
	}
}
