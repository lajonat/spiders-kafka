package com.similargroup.DIConsumer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Holds data on tracker requests
 * Created by jonathan on 5/6/2015.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrackerData{
	public String sourceId;
	public String subId;
	public String pid;
	public String sessionId;
	public String ipAddress;
	public String isOneWay;
	public String hreferer;
	public String httpCode;
	public String prev;
	public String q;
	public String httpRedierceToUrl;
	public String link;
	public Long kafkaTime;
	public Long requestTime;
	public String userAgent;
	public String countryCode;
	public String contentType;
	public String application;
	public String version;
	public String isFromBrowser;
	public String trackingModuleVersion;
	public String tracker;
	public String ipHash;
}
