package com.similargroup.DIConsumer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Holds data on tracker requests
 * Created by jonathan on 5/6/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AdvTrackerData extends TrackerData{
	public String clientRedirect;
	public String clientRedirectDuration;
	public String serverRedirect;
}
