package com.similargroup.DIConsumer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.similargroup.DIConsumer.base.BaseProcessor;
import com.similargroup.DIConsumer.base.KafkaMessageException;
import com.similargroup.DIConsumer.utils.*;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.util.ShutdownHookManager;
import org.apache.log4j.Logger;
import org.msgpack.jackson.dataformat.MessagePackFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;

/**
* Reads mobile messages, transforms 1004 events to TSV - SAVES TO FTP
* Created by jonathan on 4/30/2015.
*/
public class MobileHistoryConsumer {
	private static Logger log = Logger.getLogger("kafkalog");

	private static KafkaListener listener;

	private static final String TOPIC_DEFAULT= "mobile";
	private static final String GROUP_DEFAULT= "mobile-history";
	private static final String BASEDIR_DEFAULT = "/datatemp/exportmobile/history/";
	private static final int THREAD_COUNT_DEFAULT= 1;

	private static final String CONFIG_PATH_BASE = "/services/data-ingestion/";
	private static final String CONFIG_PATH = CONFIG_PATH_BASE + "kafka-consumers/mobilehistory/";
	private static final String TOPIC_KEY = CONFIG_PATH+"topic";
	private static final String GROUP_KEY = CONFIG_PATH+"group";
	private static final String THREADS_KEY = CONFIG_PATH+"threads";
	private static final String BASEDIR_KEY = CONFIG_PATH+"basedir";

	private static final String TRACKER_CONFIG_PATH = CONFIG_PATH_BASE + "trackers/";

	private static final String TAB= "\t";

	private static class Arguments {
		@Argument(alias = "c", description = "Path to HDFS config dir")
		String config;
		@Argument(alias = "e", description = "Environment to run in (default is production, other option is staging)")
		String env = "production";
		@Argument(alias = "p", description = "Port to listen for status requests")
		Integer statusPort = StatusUtil.DEFAULT_PORT;
	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		Logs.Init();

		final Arguments arguments = new Arguments();
		Args.parseOrExit(arguments, args, true);
		StatusUtil.setPort(arguments.statusPort);

		// if this will not be done before first call to ConfigUtil.GetInstance the default 'production' environment would be used.
		ConfigUtil.SetEnvironment(arguments.env);
		if (StringUtils.isNotBlank(arguments.config)) {
		  HDFSFileUtil.SetConfigDir(arguments.config);
		}

		MobileHistoryProcessor processor = new MobileHistoryProcessor();

		ConfigUtil conf = ConfigUtil.getInstance();

		listener = new KafkaListener(processor,
				conf.getConfig(TOPIC_KEY, TOPIC_DEFAULT),
				conf.getConfig(GROUP_KEY, GROUP_DEFAULT),
				conf.getConfigAsInt(THREADS_KEY, THREAD_COUNT_DEFAULT));

		listener.Start();

		// Attach to HDFS shutdown hook - we don't use the general shutdown hook because there's no guarantee on order,
		// So what happened was that the HDFS shutdown happened first, closed all sockets, then the HDFS Util shutdown happened,
		// And tried to flush file buffers to already closed sockets. This way, we can ensure that the util shutdown happens before the HDFS shutdown,
		// By using a higher priority
		ShutdownHookManager.get().addShutdownHook(new Runnable() {
			@Override
			public void run() {
				listener.Shutdown();
				try {
					HDFSFileUtil.GetInstance().Shutdown();
				} catch (IOException e) {
					log.fatal("Could not connect to HDFS when shutting down", e);
				} catch (URISyntaxException e) {
					log.fatal("HDFS URI invalid!", e);
				}

				ConfigUtil.Shutdown();
				StatusUtil.Shutdown();
			}
		}, FileSystem.SHUTDOWN_HOOK_PRIORITY * 10);
	}

	/**
	 * MobileHistoryProcessor receives mobile records, takes only 1004 events, and writes them out as TSV
	 */
	public static class MobileHistoryProcessor extends BaseProcessor {

		private ObjectMapper mapper = new ObjectMapper(new MessagePackFactory());

		// Create file path and name
		private final ConcurrentDateFormatAccess dirFormat = new ConcurrentDateFormatAccess("yy_MM_dd");
		private final ConcurrentDateFormatAccess fileFormat = new ConcurrentDateFormatAccess("yyyy-MM-dd_HHmmss");

		public MobileHistoryProcessor() {
			super("kafka.consumer.mobilehistory." + Logs.getHostName());
		}

		@Override
		public void Process(byte[] data, int threadNum) throws InterruptedException, KafkaMessageException {

			// Serialize message to mobile record object
			MobileData message;
			try {
				message = mapper.readValue(data, MobileData.class);
			} catch (IOException e) {
				log.error("Could not parse read message", e);
				incrementCounter("messages.error");
				throw new KafkaMessageException(e);
			}

			// If message is not 1004, discard it and continue to the next one
			if (message.t != 1004){
				return;
			}
			incrementCounter("messages.read");

			// Get message timestamp, calculate lag
			long messageTimestamp = calcLag(message.kafkaTime);
			Date dt = new Date(messageTimestamp);

			// IMPORTANT: IF WE HAVE A TRACKER FIELD, WE HAVE TO MARK THE TRACKER AS DONE WITH THE PREVIOUS DAY
			if (StringUtils.isNotBlank(message.tracker)){
				setTrackerDayFinished(TRACKER_CONFIG_PATH,ConfigUtil.getInstance().getConfig(GROUP_KEY, GROUP_DEFAULT), message.tracker, dt);
			}

			// Create TSV lines
			String result;
			String common = fixString(message.v, true) + TAB +
					fixString(message.os, true) + TAB +
					fixString(message.model, true) + TAB +
					message.ts_gmt + TAB +
					message.t + TAB +
					fixString(message.s, true) + TAB +
					message.ts + TAB +
					message.r + TAB +
					fixString(message.osv, true) + TAB +
					fixString(message.did, true) + TAB +
					fixString(message.osv, true) + TAB +
					fixString(message.cv, true) + TAB +
					fixString(message.Country, true) + TAB +
					fixString(message.ip, true) + TAB;

			// If no history given, write out "still alive" row
			if (message.hist.length == 0) {
				result = common + "0\t\talive\t\t" + fixString(message.ipHash, true) + "\n"; // ts	browser	q	bm
			}else{
				// Create a row for each history event
				StringBuilder lines = new StringBuilder();
				for (int i = 0; i < message.hist.length; i++) {
					HistData hist = message.hist[i];
					lines.append(common).
							append(hist.ts).append(TAB).
							append(fixString(hist.browser, true)).append(TAB).
							append(fixString(hist.q, true)).append(TAB).
							append(hist.bm).append(TAB).
							append(fixString(message.ipHash, true)).append("\n");
				}
				result = lines.toString();
			}

			try {
				// Write to HDFS - wait before and after if HDFS is invalid - this is so that if there's a problem, we won't be fetching more messages
				// (And incrementing the topic queue head) that can't be written to HDFS, and will be lost
				ensureHDFS();
				HDFSFileUtil.GetInstance().write(String.format("%s%s/stats_%s", ConfigUtil.getInstance().getConfig(BASEDIR_KEY, BASEDIR_DEFAULT), message.s, dirFormat.convertDateToString(dt)),
						String.format("stat_%s_%s.dat.gz", fileFormat.convertDateToString(dt), hostname), result);
				incrementCounter("messages.written");
				ensureHDFS();

			} catch (IOException e) {
				log.fatal("Could not connect to HDFS!", e);
				StatusUtil.getInstance().setStatus(StatusUtil.Status.Down, "Problem with HDFS access: " + e.getMessage());
			} catch (URISyntaxException e) {
				log.fatal("HDFS URI invalid!", e);
				StatusUtil.getInstance().setStatus(StatusUtil.Status.Down, "HDFS URL invalid! " + e.getMessage());
			}
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class MobileData{
		public Long kafkaTime;
		public String v;
		public String os;
		public String model;
		public Long ts_gmt;
		public int t;
		public String s;
		public Long ts;
		public boolean r;
		public String osv;
		public String did;
		public String cv;
		public String Country;
		public String ip;
		public String ipHash;
		public String tracker;
		public HistData[] hist;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class HistData{
		public Long ts;
		public String browser;
		public String q;
		public boolean bm;
	}
}
