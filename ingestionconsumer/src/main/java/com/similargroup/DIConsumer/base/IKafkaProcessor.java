package com.similargroup.DIConsumer.base;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Defines kafka consumer logic - each target has a different implementation for processing kafka messages
 * Created by jonathan on 4/30/2015.
 */
public interface IKafkaProcessor {
	void Process(byte[] data, int threadNum) throws InterruptedException, KafkaMessageException;
}
