package com.similargroup.DIConsumer.base;

/**
 * Signals that an error has been reached unexpectedly while parsing message from kafka.
 * Created by jonathan on 5/18/2015.
 */
public class KafkaMessageException extends Exception{
	public KafkaMessageException(String message) {
		super(message);
	}

	public KafkaMessageException(Throwable cause) {
		super("Could not read message", cause);
	}

	public KafkaMessageException(String message, Throwable cause) {
		super(message, cause);
	}
}

