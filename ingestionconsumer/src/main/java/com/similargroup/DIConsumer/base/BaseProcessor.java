package com.similargroup.DIConsumer.base;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.TokenBuffer;
import com.similargroup.DIConsumer.utils.ConfigUtil;
import com.similargroup.DIConsumer.utils.Constants;
import com.similargroup.DIConsumer.utils.HDFSFileUtil;
import com.similargroup.DIConsumer.utils.StatusUtil;
import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.msgpack.jackson.dataformat.MessagePackFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Base class for processor, holds shared functions
 * Created by jonathan on 5/6/2015.
 */
public abstract class BaseProcessor implements IKafkaProcessor{
	private static Logger log = Logger.getLogger("kafkalog");
	private static final StatsDClient statsd = new NonBlockingStatsDClient("kafka.consumer.daychange.", Constants.GRAPHITE_HOST, Constants.GRAPHITE_PORT);

	private static final Random rnd = new Random();
	protected StatsDClient specificStatsd;	// Client for specific processor - to differentiate the prefix
	protected GraphiteReporter directGraphite;	// Client for direct graphite reporting - for lag

	protected String hostname;

	protected final char[] fieldEndChars = new char[]{',', '}'};
	private final DateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd");

	protected long lag = 0; // Determines how much behind we are on receiving messages
	private Timer lagtimer;	// Periodic checks to reset lag
	protected long lagReductionFactor = 1; // How many seconds to reduce from lag each tick - grows exponentially

	abstract public void Process(byte[] data, int threadNum) throws InterruptedException, KafkaMessageException;

	public BaseProcessor(String statsdPrefix){
		specificStatsd = new NonBlockingStatsDClient(statsdPrefix, Constants.GRAPHITE_HOST, Constants.GRAPHITE_PORT);
		try {
			hostname = java.net.InetAddress.getLocalHost().getHostName().replace(".sg.internal","");
		} catch (UnknownHostException e) {
			log.error("Could not get hostname, using default",e);
			hostname = "Unknown";
		}
		lagtimer = new Timer(true);
		lagtimer.scheduleAtFixedRate(new resetTimer(), 0, 10000);

		// Create a gauge on the lag metric
		MetricRegistry metrics = new MetricRegistry();
		Gauge<Long> gauge = new Gauge<Long>() {
			@Override
			public Long getValue() {return lag;}
		};
		metrics.register("lag", gauge);

		// Report to graphite every 10 seconds
		Graphite g = new Graphite(Constants.DIRECT_GRAPHITE_HOST, Constants.DIRECT_GRAPHITE_PORT);
		directGraphite = GraphiteReporter.forRegistry(metrics).prefixedWith("stats.gauges." + statsdPrefix).
				convertRatesTo(TimeUnit.SECONDS).
				convertDurationsTo(TimeUnit.MILLISECONDS).
				filter(MetricFilter.ALL).build(g);

		directGraphite.start(10, TimeUnit.SECONDS);
	}

	private class resetTimer extends TimerTask {

		@Override
		public void run() {
			lag = Math.max(0, lag - (lagReductionFactor*1000));
			lagReductionFactor *= 2;
		}
	}

	/**
	 * Increments the given counter on statsd - sampled
	 * @param counter - the counter key to increment
	 */
	protected void incrementCounter(String counter){
		// Sample rate of 1%
		if (rnd.nextDouble() <= 0.01){
			specificStatsd.count(counter, 1, 0.01);
		}
	}

	/**
	 * Calculates and logs message lag
	 * @param messageTimestamp - timestamp of current message
	 * @return the real timestamp of the message
	 */
	protected long calcLag(long messageTimestamp) {
		Date dt = new Date();
		long now = dt.getTime();

		long newLag = now - messageTimestamp;
		if (newLag > this.lag) {
			this.lag = Math.max(0, newLag);
			lagReductionFactor = 1;
		}

		return messageTimestamp;
	}

	/**
	 * Ensures that the HDFS is in a valid state - if not, it waits until it is
	 */
	protected void ensureHDFS() throws InterruptedException {
		boolean OK = false;
		do {
			// If interrupted thread, we're shutting down
			if (Thread.interrupted()){
				throw new InterruptedException("Cancelling thread...");
			}

			// Only if HDFS is usable, we stop checking
			try {
				if (HDFSFileUtil.GetInstance().isUsable()) {
					OK = true;
				}
			}catch (IOException e) {
				log.fatal("Could not connect to HDFS!", e);
				StatusUtil.getInstance().setStatus(StatusUtil.Status.Down, "Problem with HDFS access:" + e.getMessage());
			} catch (URISyntaxException e) {
				log.fatal("HDFS URI invalid!", e);
				StatusUtil.getInstance().setStatus(StatusUtil.Status.Down, "HDFS URL invalid! " + e.getMessage());
			}

			if (!OK) {
				//TODO:LOG!ALERT!
				log.error("HDFS not usable!");
				Thread.sleep(5000);
			}
		} while (!OK);
	}

	/**
	 * Decodes a messagepacked json to string
	 * @param data - message packed json data
	 * @param factory - message pack factory for decoding data
	 * @param mapper - object mapper to write JSON to string
	 * @return - string representation of the JSON data
	 * @throws IOException
	 */
	protected String getJsonStringFromMsgpack(byte[] data, MessagePackFactory factory, ObjectMapper mapper) throws IOException {
		String message;// Read json tokens to buffer
		JsonParser pa = factory.createParser(data);
		TokenBuffer buf = new TokenBuffer(pa);
		pa.nextToken();
		buf.copyCurrentStructure(pa);
		pa.close();

		// Write as JSON text
		StringWriter w = new StringWriter();
		WriterOutputStream sw = new WriterOutputStream(w, "UTF-8");
		mapper.writeValue(sw, buf);
		message = fixString(w.toString(), false) + "\n";
		sw.close();
		return message;
	}

	/**
	 * Formats string to TSV compatible
	 * @param value - String to format
	 * @param isTSV - Whether to format for TSV or not
	 * @return - formatted string
	 */
	protected String fixString(String value, boolean isTSV){
		if (value == null){
			return "";
		}

		if (isTSV) {
			return StringUtils.replaceEach(value, new String[]{"\t", "\n", "\r"}, new String[]{"", "", ""});
		} else{
			return StringUtils.replaceEach(value, new String[]{"\n", "\r"}, new String[]{"", ""});
		}
	}

	/**
	 * Extracts a JSON field from a string representation
	 * @param json - the JSON string that holds the data
	 * @param field - the field name to look for
	 * @param isString - whether the wanted field is a string (enclosed in ")
	 * @return The string value of the field, if found
	 * @throws IOException - Did not find field
	 */
	protected String getJSONField(String json, String field, boolean isString) throws IOException {
		String fullField = "\"" + field + "\":";
		if (isString){
			fullField += "\"";
		}
		try {
			int timestampStart = json.indexOf(fullField);
			if (timestampStart == -1){
				return null;
			}

			timestampStart += fullField.length();
			int timesttampEnd = StringUtils.indexOfAny(json.substring(timestampStart), fieldEndChars);

			String found = json.substring(timestampStart, timestampStart+timesttampEnd);
			if (isString){
				found = found.substring(0, found.length() - 1);
			}
			return found;
		} catch (Exception e){
			return null;
		}
	}

	/**
	 * Marks a specific tracker as done with previous day - if we got a message in day x, then we can be sure that the tracker is done with day x-1
	 * This marks the tracker with the given date in the ETCD config path given
	 * EACH IMPLEMENTATION MUST CALL THIS METHOD WHEN WE'RE SURE THAT A TRACKER IS DONE WITH A DAY, OTHERWISE BIGDATA WON'T KNOW WHEN TO START
	 * @param configKey - Base config path for marking trackers
	 * @param group - Consumer group to mark for - each group is marked individually
	 * @param trackerID - The name of the tracker that gave this message
	 * @param messageTimestamp - Timestamp of flag message - this time -1 day is the date we're sure we're done with
	 */
	protected void setTrackerDayFinished(String configKey, String group, String trackerID, Date messageTimestamp){
		Calendar c = Calendar.getInstance();
		c.setTime(messageTimestamp);
		c.add(Calendar.DATE, -1);
		String configValue = dayFormat.format(c.getTime());

		ConfigUtil.getInstance().setConfig(configKey + group + "/" + trackerID, configValue);

		Date now = new Date();
		c.setTime(now);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		long daychangeTime = c.getTime().getTime();
		statsd.time(StringUtils.replaceChars(trackerID, '.', '_'), now.getTime() - daychangeTime);
	}
}
