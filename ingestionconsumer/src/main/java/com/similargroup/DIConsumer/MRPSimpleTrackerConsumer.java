package com.similargroup.DIConsumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.similargroup.DIConsumer.base.BaseProcessor;
import com.similargroup.DIConsumer.base.KafkaMessageException;
import com.similargroup.DIConsumer.utils.*;
import com.similargroup.common.net.UrlZ;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.util.ShutdownHookManager;
import org.apache.log4j.Logger;
import org.msgpack.jackson.dataformat.MessagePackFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
* Reads tracker messages, transforms to TSV - SAVE TO MRP
* Created by jonathan on 5/06/2015.
*/
public class MRPSimpleTrackerConsumer {

	private static Logger log = Logger.getLogger("kafkalog");

	private static KafkaListener listener;
	private static List<String> ppcRules;

	private static final String TOPIC_DEFAULT= "tracker";
	private static final String PPC_RULES_DEFAULT= "gclid";
	private static final String GROUP_DEFAULT= "mrpsimpletracker";
	private static final String BASEDIR_DEFAULT = "/temp/similargroup/data/raw-stats-paid/";
	private static final int THREAD_COUNT_DEFAULT= 1;

	private static final String CONFIG_PATH_BASE = "/services/data-ingestion/";
	private static final String CONFIG_PATH = CONFIG_PATH_BASE + "kafka-consumers/mrpsimpletracker/";
	private static final String TOPIC_KEY = CONFIG_PATH+"topic";
	private static final String PPC_RULES_KEY = CONFIG_PATH+"ppcrules";
	private static final String GROUP_KEY = CONFIG_PATH+"group";
	private static final String THREADS_KEY = CONFIG_PATH+"threads";
	private static final String BASEDIR_KEY = CONFIG_PATH+"basedir";

	private static final String TRACKER_CONFIG_PATH = CONFIG_PATH_BASE + "trackers/";

	private static final String TAB= "\t";

	private static class Arguments {
		@Argument(alias = "c", description = "Path to HDFS config dir")
		String config;
		@Argument(alias = "e", description = "Environment to run in (default is production, other option is staging)")
		String env = "production";
		@Argument(alias = "p", description = "Port to listen for status requests")
		Integer statusPort = StatusUtil.DEFAULT_PORT;
	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		log.fatal("Starting...");
		Logs.Init();

		final Arguments arguments = new Arguments();
		Args.parseOrExit(arguments, args, true);
		StatusUtil.setPort(arguments.statusPort);

		// if this will not be done before first call to ConfigUtil.GetInstance the default 'production' environment would be used.
		ConfigUtil.SetEnvironment(arguments.env);
		if (StringUtils.isNotBlank(arguments.config)) {
			HDFSFileUtil.SetConfigDir(arguments.config);
		}

		TrackerProcessor processor = new TrackerProcessor();

		ConfigUtil conf = ConfigUtil.getInstance();

		listener = new KafkaListener(processor,
				conf.getConfig(TOPIC_KEY, TOPIC_DEFAULT),
				conf.getConfig(GROUP_KEY, GROUP_DEFAULT),
				conf.getConfigAsInt(THREADS_KEY, THREAD_COUNT_DEFAULT));

		listener.Start();


		String ppcRulesString = conf.getConfig(PPC_RULES_KEY, PPC_RULES_DEFAULT);
		ppcRules = Arrays.asList(ppcRulesString.toLowerCase().split("\\s*,\\s*"));

		// Attach to HDFS shutdown hook - we don't use the general shutdown hook because there's no guarantee on order,
		// So what happened was that the HDFS shutdown happened first, closed all sockets, then the HDFS Util shutdown happened,
		// And tried to flush file buffers to already closed sockets. This way, we can ensure that the util shutdown happens before the HDFS shutdown,
		// By using a higher priority
		ShutdownHookManager.get().addShutdownHook(new Runnable() {
			@Override
			public void run() {
				listener.Shutdown();
				try {
					HDFSFileUtil.GetInstance().Shutdown();
				} catch (IOException e) {
					log.fatal("Could not connect to HDFS when shutting down", e);
				} catch (URISyntaxException e) {
					log.fatal("HDFS URI invalid!", e);
				}
				ConfigUtil.Shutdown();
				StatusUtil.Shutdown();
			}
		}, FileSystem.SHUTDOWN_HOOK_PRIORITY * 10);

//		log.fatal("Testing...");
//		processor.Test("https://hello.com?bob=4388",
//				"https://google.com/",
//				"https://google.com/");
//		processor.Test("https://hello.com?gclid=stevo",
//				"https://google.co.il/",
//				"https://google.co.il/");
//		processor.Test("http://fr.aliexpress.com/item/Children-Heelys-Child-Double-Wheels-Roller-Shoes-Boys-Girls-Flying-Shoes-Kids-Rollerskate-Sneakers-3-Colors/32432809954.html?plac&gclid=CP7SlYrV0McCFUkUwwodWvsFxw&mtctp&trgt=18283950120&albag=17814773401&netw&albcp=287465161&isdl=y&currencyType=EUR&crea=fr32432809954&aff_short_key=UneMJZVf&albch=shopping&slnk&device=c&src=google&acnt=494-037-6276",
//				"https://google.ru/",
//				"https://google.ru/");
//		processor.Test("https://hello.com?%7Bkeyword%7D=value",
//				"https://google.fr/",
//				"https://google.com/");
//		processor.Test("https://hello.com?gclid=true",
//				"https://googlefake.com/",
//				"https://googlefake.com/");
//		processor.Test("https://hello.com?notgclid=true",
//				"https://google.it/",
//				"https://google.it/");
//		processor.Test("http://ru.aliexpress.com/ru_home.htm?src=google&albch=google&acnt=304-410-9721&isdl=y&aff_short_key=UneMJZVf&albcp=54095668&albag=1897140028&slnk=&trgt=kwd-23810209708&plac=&crea=67298338828&netw=g&device=c&mtctp=b&gclid=Cj0KEQjw04qvBRC6vfKG2Pi0_8gBEiQAAJq0vVRf6ZjRFezbjzZNKGO4vbh1iIj5Bm-LR-m5987Fh0UaAr6G8P8HAQ",
//				"https://google.ly/",
//				"https://google.ly/");
//		processor.Test("https://hello.com?no=psite&me=751",
//				"https://google.br?q=world",
//				"https://google.br?q=hello");
//		processor.Test("https://google.com?gclid=true",
//				"https://google.com?q=world",
//				"https://google.com?q=hello");
//		log.fatal("Testing... 2");
	}

	/**
	 * applyAclkRules receives a message and adds aclk to the referrer in-case we believe it was a PPC click
	 * This is a hack that will be removed once the big data finished the advanced tracker and apply these rules
	 * while digesting.
	 */
	private static void applyAclkRules(TrackerData message) {
		try {
				boolean referrerHasAclk = message.hreferer.contains("aclk");
				if(!referrerHasAclk) {
					UrlZ referrer = new UrlZ(message.hreferer);
					if (referrer.getMainDomain().equals("google")) {
						UrlZ prev = new UrlZ(message.prev);
						if (prev.getMainDomain().equals("google")) {
							UrlZ current = new UrlZ(message.q);
							if (!current.getMainDomain().equals("google")) {
								String[] queryParams = current.getQueryParams();
								if (queryParams != null) {
									for (String queryParam : queryParams) {
										String[] keyValue = queryParam.split("=");
										for (String param : keyValue) {
											for (String ppcRule : ppcRules) {
												if (param.equals(ppcRule)) {
													message.hreferer = "https://" + referrer.getShortUrl() + "/aclk?injected=true";
													return;
												}
											}
										}
									}
								}
							}
						}
					}
			}
		} catch (MalformedURLException e) {
			log.fatal("MalformedURLException");
		}
	}

	/**
	 * TrackerProcessor receives old tracker records and writes them out as TSV
	 */
	public static class TrackerProcessor extends BaseProcessor {

		private ObjectMapper mapper = new ObjectMapper(new MessagePackFactory());

		// Create file path and name
		private final ConcurrentDateFormatAccess dirFormat = new ConcurrentDateFormatAccess("'year='yy'/month='MM'/day='dd");
		private final ConcurrentDateFormatAccess fileFormat = new ConcurrentDateFormatAccess("yyyy-MM-dd_HHmmss");

		public TrackerProcessor() {
			super("kafka.consumer.mrpsimpletracker." + Logs.getHostName());
		}


		public void Test(String query, String ref, String prev) {
			TrackerData message = new TrackerData();
			log.info("??????????");
			log.info("REF: " + ref);
			log.info("PRV: " + prev);
			log.info("QRY: " + query);
			message.sourceId = "861";
			message.subId = "chrome";
			message.pid = "ABCD-EFGH-1234-5678";
			message.sessionId = "1234-5678-ABCD-EFGH";
			message.ipAddress = "127.0.0.1";
			message.isOneWay = "0";
			message.hreferer = ref;
			message.httpCode = "200";
			message.prev = prev;
			message.q = query;
			message.httpRedierceToUrl = "0";
			message.link = "0";
			message.kafkaTime = 1445514555l;
			message.requestTime = 1445514444l;
			message.userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36";
			message.countryCode = "840";
			message.contentType = "0";
			message.application = "0";
			message.version = "1.0";
			message.isFromBrowser = "0";
			message.trackingModuleVersion = "1.1";
			message.tracker = "test";
			message.ipHash = "abcdefg";

			byte[] bytes;
			try {
				bytes = mapper.writeValueAsBytes(message);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
				return;
			}

			try {
				this.Process(bytes, 1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (KafkaMessageException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void Process(byte[] data, int threadNum) throws InterruptedException, KafkaMessageException {

			// Serialize message to tracker record object
			TrackerData message;
			try {
				message = mapper.readValue(data, TrackerData.class);
			} catch (IOException e) {
				log.error("Could not parse read message", e);
				incrementCounter("messages.error");
				throw new KafkaMessageException(e);
			}

			incrementCounter("messages.read");

			// Get message timestamp, calculate lag
			long messageTimestamp = calcLag(message.kafkaTime);
			Date dt = new Date(messageTimestamp);

			// IMPORTANT: IF WE HAVE A TRACKER FIELD, WE HAVE TO MARK THE TRACKER AS DONE WITH THE PREVIOUS DAY
			if (StringUtils.isNotBlank(message.tracker)){
				setTrackerDayFinished(TRACKER_CONFIG_PATH,ConfigUtil.getInstance().getConfig(GROUP_KEY, GROUP_DEFAULT), message.tracker, dt);
			}

			// aclk is dead, this is a hack to revive it.
			// Lets see if the referrer might be google, just to filter the big mass of events out of the way
			boolean refererIsLikelyGoogle = message.hreferer.contains("google");
			if(refererIsLikelyGoogle) {
				boolean prevIsLikelyGoogle = message.prev.contains("google");
				if(prevIsLikelyGoogle) {
					applyAclkRules(message);
				}
				else {
					log.debug("Checking basic ppc rule - no google");
				}
			}


			// Create TSV lines
			String line =
					String.format("%.3f%s",(message.requestTime / 1.0),TAB) +
					fixString(message.sourceId, true) + TAB +
					fixString(message.subId, true) + TAB +
					message.pid + TAB +
					fixString(message.sessionId, true) + TAB +
					fixString(message.ipAddress, true) + TAB +
					fixString(message.countryCode, true) + TAB +
					fixString(message.userAgent, true) + TAB +
					fixString(message.isOneWay, true) + TAB +
					fixString(message.link, true) + TAB +
					fixString(message.hreferer, true) + TAB +
					fixString(message.prev, true) + TAB +
					fixString(message.q, true) + TAB +
					fixString(message.httpCode, true) + TAB +
					fixString(message.httpRedierceToUrl, true) + TAB +
					fixString(message.contentType, true) + TAB +
					fixString(message.application, true) + TAB +
					fixString(message.isFromBrowser, true) + TAB +
					fixString(message.version, true) + TAB +
							TAB +
							TAB +
					fixString(message.trackingModuleVersion, true) + TAB +
							TAB +
					fixString(message.ipHash, true)	+"\n";


//			log.info("*****");
//			log.info("*****");
//			log.info("ref " + message.hreferer);
//			log.info("prv " + message.prev);
//			log.info("qry " + message.q);

			try {
				// Write to HDFS - wait before and after if HDFS is invalid - this is so that if there's a problem, we won't be fetching more messages
				// (And incrementing the topic queue head) that can't be written to HDFS, and will be lost
				ensureHDFS();
				HDFSFileUtil.GetInstance().write(String.format("%s%s/app=%s", ConfigUtil.getInstance().getConfig(BASEDIR_KEY, BASEDIR_DEFAULT), dirFormat.convertDateToString(dt), hostname),
						String.format("%s_%s.dat.gz", fileFormat.convertDateToString(dt), hostname), line, false);
				incrementCounter("messages.written");
				ensureHDFS();

			} catch (IOException e) {
				log.fatal("Could not connect to HDFS!", e);
				StatusUtil.getInstance().setStatus(StatusUtil.Status.Down, "Problem with HDFS access: " + e.getMessage());
			} catch (URISyntaxException e) {
				log.fatal("HDFS URI invalid!", e);
				StatusUtil.getInstance().setStatus(StatusUtil.Status.Down, "HDFS URL invalid! " + e.getMessage());
			}
		}
	}
}
