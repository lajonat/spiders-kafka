package com.similargroup.DIConsumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.similargroup.DIConsumer.base.BaseProcessor;
import com.similargroup.DIConsumer.base.KafkaMessageException;
import com.similargroup.DIConsumer.utils.*;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.util.ShutdownHookManager;
import org.apache.log4j.Logger;
import org.msgpack.jackson.dataformat.MessagePackFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;

/**
* Reads advanced tracker messages transforms to TSV - SAVES TO FTP
* Created by jonathan on 5/06/2015.
*/
public class FTPAdvTrackerConsumer {

	private static Logger log = Logger.getLogger("kafkalog");

	private static KafkaListener listener;

	private static final String TOPIC_DEFAULT= "advanced_tracker";
	private static final String GROUP_DEFAULT= "ftpadvtracker";
	private static final String BASEDIR_DEFAULT = "/datatemp/exportadv/";
	private static final int THREAD_COUNT_DEFAULT= 1;

	private static final String CONFIG_PATH_BASE = "/services/data-ingestion/";
	private static final String CONFIG_PATH = CONFIG_PATH_BASE + "kafka-consumers/ftpadv-tracker/";
	private static final String TOPIC_KEY = CONFIG_PATH + "topic";
	private static final String GROUP_KEY = CONFIG_PATH + "group";
	private static final String THREADS_KEY = CONFIG_PATH + "threads";
	private static final String BASEDIR_KEY = CONFIG_PATH + "basedir";

	private static final String TRACKER_CONFIG_PATH = CONFIG_PATH_BASE + "trackers/";

	private static final String TAB= "\t";

	private static class Arguments {
		@Argument(alias = "c", description = "Path to HDFS config dir")
		String config;
		@Argument(alias = "e", description = "Environment to run in (default is production, other option is staging)")
		String env = "production";
		@Argument(alias = "p", description = "Port to listen for status requests")
		Integer statusPort = StatusUtil.DEFAULT_PORT;
	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		Logs.Init();
		final Arguments arguments = new Arguments();
		Args.parseOrExit(arguments, args, true);
		StatusUtil.setPort(arguments.statusPort);

		ConfigUtil.SetEnvironment(arguments.env);

		if (StringUtils.isNotBlank(arguments.config)) {
			HDFSFileUtil.SetConfigDir(arguments.config);
		}

		AdvTrackerProcessor processor = new AdvTrackerProcessor();

		ConfigUtil conf = ConfigUtil.getInstance();

		listener = new KafkaListener(processor,
				conf.getConfig(TOPIC_KEY, TOPIC_DEFAULT),
				conf.getConfig(GROUP_KEY, GROUP_DEFAULT),
				conf.getConfigAsInt(THREADS_KEY, THREAD_COUNT_DEFAULT));

		listener.Start();

		// Attach to HDFS shutdown hook - we don't use the general shutdown hook because there's no guarantee on order,
		// So what happened was that the HDFS shutdown happened first, closed all sockets, then the HDFS Util shutdown happened,
		// And tried to flush file buffers to already closed sockets. This way, we can ensure that the util shutdown happens before the HDFS shutdown,
		// By using a higher priority
		ShutdownHookManager.get().addShutdownHook(new Runnable() {
			@Override
			public void run() {
				listener.Shutdown();
				try {
					HDFSFileUtil.GetInstance().Shutdown();
				} catch (IOException e) {
					log.fatal("Could not connect to HDFS when shutting down", e);
				} catch (URISyntaxException e) {
					log.fatal("HDFS URI invalid!", e);
				}
				ConfigUtil.Shutdown();
				StatusUtil.Shutdown();
			}
		}, FileSystem.SHUTDOWN_HOOK_PRIORITY * 10);
	}

	/**
	 * AdvTrackerProcessor receives advanced tracker records and writes them out as TSV
	 */
	public static class AdvTrackerProcessor extends BaseProcessor {

		private ObjectMapper mapper = new ObjectMapper(new MessagePackFactory());

		// Create file path and name
		private final ConcurrentDateFormatAccess dirFormat = new ConcurrentDateFormatAccess("yy_MM_dd");
		private final ConcurrentDateFormatAccess fileFormat = new ConcurrentDateFormatAccess("yyyy-MM-dd_HHmmss");

		public AdvTrackerProcessor() {
			super("kafka.consumer.ftpadvtracker." + Logs.getHostName());
		}

		@Override
		public void Process(byte[] data, int threadNum) throws InterruptedException, KafkaMessageException {

			// Serialize message to tracker record object
			AdvTrackerData message;
			try {
				message = mapper.readValue(data, AdvTrackerData.class);
			} catch (IOException e) {
				log.error("Could not parse read message", e);
				incrementCounter("messages.error");
				throw new KafkaMessageException(e);
			}
			incrementCounter("messages.read");

			// Get message timestamp, calculate lag
			long messageTimestamp = calcLag(message.kafkaTime);
			Date dt = new Date(messageTimestamp);

			// IMPORTANT: IF WE HAVE A TRACKER FIELD, WE HAVE TO MARK THE TRACKER AS DONE WITH THE PREVIOUS DAY
			if (StringUtils.isNotBlank(message.tracker)){
				setTrackerDayFinished(TRACKER_CONFIG_PATH, ConfigUtil.getInstance().getConfig(GROUP_KEY, GROUP_DEFAULT), message.tracker, dt);
			}

			// Create TSV lines
			String line =
					String.format("%.3f%s",(message.kafkaTime / 1000.0),TAB) +
					fixString(message.sourceId, true) + TAB +
					fixString(message.subId, true) + TAB +
					message.pid + TAB +
					fixString(message.sessionId, true) + TAB +
					fixString(message.ipAddress, true) + TAB +
					fixString(message.countryCode, true) + TAB +
					fixString(message.userAgent, true) + TAB +
					fixString(message.isOneWay, true) + TAB +
					fixString(message.link, true) + TAB +
					fixString(message.hreferer, true) + TAB +
					fixString(message.prev, true) + TAB +
					fixString(message.q, true) + TAB +
					fixString(message.httpCode, true) + TAB +
					fixString(message.httpRedierceToUrl, true) + TAB +
					fixString(message.contentType, true) + TAB +
					fixString(message.application, true) + TAB +
					fixString(message.isFromBrowser, true) + TAB +
					fixString(message.version, true) + TAB +
					fixString(message.clientRedirect, true) + TAB +
					fixString(message.clientRedirectDuration, true) + TAB +
					fixString(message.trackingModuleVersion, true) + TAB +
					fixString(message.serverRedirect, true) + TAB +
					fixString(message.ipHash, true)	+ "\n";


			try {
				// Write to HDFS - wait before and after if HDFS is invalid - this is so that if there's a problem, we won't be fetching more messages
				// (And incrementing the topic queue head) that can't be written to HDFS, and will be lost
				ensureHDFS();
				HDFSFileUtil.GetInstance().write(String.format("%s%s/stats_%s", ConfigUtil.getInstance().getConfig(BASEDIR_KEY, BASEDIR_DEFAULT), message.sourceId, dirFormat.convertDateToString(dt)),
						String.format("stat_%s_%s.dat.gz", fileFormat.convertDateToString(dt), hostname), line);
				incrementCounter("messages.written");
				ensureHDFS();

			} catch (IOException e) {
				log.fatal("Could not connect to HDFS!", e);
				StatusUtil.getInstance().setStatus(StatusUtil.Status.Down, "Problem with HDFS access: " + e.getMessage());
			} catch (URISyntaxException e) {
				log.fatal("HDFS URI invalid!", e);
				StatusUtil.getInstance().setStatus(StatusUtil.Status.Down, "HDFS URL invalid! " + e.getMessage());
			}
		}
	}
}
