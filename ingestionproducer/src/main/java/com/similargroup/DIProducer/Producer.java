package com.similargroup.DIProducer;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.kafka.clients.producer.*;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The producer class is a facade over a kafka client - it allows a program to send msgpacked data to kafka without knowing the details
 * The data is sent asynchroniously to kafka, as to return as fast as possible to the caller
 * Created by jonathan on 4/27/2015.
 */
public class Producer {

	private static final ExecutorService executor = Executors.newCachedThreadPool();

	private static final String ID_KEY = "kafkakey";	// Key in data that we partition on
	private static KafkaProducer<String, byte[]> producer;	// Kafka client

	private static Producer instance = null;	// Singleton instance

	// The result that is returned to the caller - if false, it means that we have problems, and it should stop receiving data.
	// This is returned asynchroniously - each call will return a result a few calls back. This way, if there is a problem, we will know about it,
	// But we don't have to wait for completion on each call
	private volatile boolean currentlyActive = true;

	private Producer(){
		// Create kafka client once
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "kafka-a01.sg.internal:9092,kafka-a02.sg.internal:9092,kafka-a03.sg.internal:9092,kafka-a04.sg.internal:9092,kafka-a05.sg.internal:9092");
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "DemoProducer");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArraySerializer");
		props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 1024*1024*1024l);
		props.put(ProducerConfig.BLOCK_ON_BUFFER_FULL_CONFIG, false);
		props.put(ProducerConfig.RETRIES_CONFIG, 3);
//		props.put(ProducerConfig.FILE_BACKED_BUFFERS_CONFIG, true);
//		props.put(ProducerConfig.FILE_BACKED_BUFFERS_FILE_NAME_CONFIG, new File(System.getProperty("java.io.tmpdir"), "kafka-producer-data.dat").getAbsolutePath());
		producer = new KafkaProducer<String, byte[]>(props);
	}

	/**
	 * Get an instance of the producer singleton
	 * @return producer instance
	 */
	public synchronized static Producer GetInstance(){
		if (instance == null){
			instance = new Producer();
		}
		return instance;
	}

	/**
	 * Send a message to the given topic
	 * @param topic - Kafka topic to save message to
	 * @param tree - Message in JSON tree
	 * @param data - Message details - messagepacked data
	 * @return True for continue, false for problem that needs to cease sending
	 */
	public boolean SendMessage(String topic, JsonNode tree, byte[] data) {

		// Run in a new thread - even though the kafka producer sends messages async, there is a metadata fetch action that
		// is synchronous and can timeout after a minute if no connection to cluster
		executor.execute(new sendRunnable(topic, tree, data));
		return currentlyActive;
	}

	/**
	 * Shuts down the producer - should be called at the end of the program, cannot be used afterwards
	 */
	public void ShutDown(){
		executor.shutdown();
		producer.close();
	}

	/**
	 * Sets current state of connectivity to kafka. Synchronized because it can be accessed multithreadedly
	 * @param state True if OK, false if failing
	 */
	private synchronized void setCurrentActive(boolean state){
		currentlyActive = state;
	}

	/**
	 * Sends a message to kafka in a thread
	 */
	private static class sendRunnable implements Runnable{

		private Random rand = new Random();

		String topic;
		JsonNode tree;
		byte[] data;
		public sendRunnable(String topic, JsonNode tree, byte[] data){
			this.topic = topic;
			this.data = data;
			this.tree = tree;
		}

		@Override
		public void run(){
			// Get key to partition on - user ID. If the field is missing, randomize
			JsonNode userID = tree.get(ID_KEY);
			String key;
			if (userID != null){
				key= userID.asText();
			}else{
				key = Integer.toString(rand.nextInt(1000000));
			}

			// Send the message with a callback, return current state
			ProducerRecord<String, byte[]> record = new ProducerRecord<String, byte[]>(topic, key, data);
			producer.send(record, new messageCallback());
		}
	}
	/**
	 * Callback for record sending to kafka
	 */
	private static class messageCallback implements Callback {
		@Override
		public void onCompletion(RecordMetadata recordMetadata, Exception e) {
			// Exactly one of the params is not null - if had exception we have a problem
			if (e != null){
				Producer.GetInstance().setCurrentActive(false);
				e.printStackTrace();
				// TODO: log error
			} else if (!Producer.GetInstance().currentlyActive) {
				// Had a problem earlier, now seems to be OK
				Producer.GetInstance().setCurrentActive(true);
			}
		}
	}
}
