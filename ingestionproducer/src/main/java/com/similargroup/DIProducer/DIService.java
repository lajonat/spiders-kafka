package com.similargroup.DIProducer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.msgpack.jackson.dataformat.MessagePackFactory;
import org.newsclub.net.unix.AFUNIXServerSocket;
import org.newsclub.net.unix.AFUNIXSocketAddress;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Entry point for sending to Kafka cluster - this is a service that listens for incoming messages over a socket, and passes them to the kafka producer
 * Created by jonathan on 4/27/2015.
 */
public class DIService {

	private static final int RESPONSE_OK = 0;
	private static final int BAD_REQUEST = 1;
	private static final int ERROR = 2;

	private static final int SERVER_INSTANCES = 16;
	private static final int MAX_MESSAGE_SIZE = 1024 * 1024 * 10;

	private static final String TOPIC_KEY = "topic";
	private static volatile boolean stopped = false;

	public static void main(String[] args) throws IOException {
		final File socketFile = new File(new File(System.getProperty("java.io.tmpdir")), "ingestion.sock");

		final AFUNIXServerSocket server = AFUNIXServerSocket.newInstance();
		server.bind(new AFUNIXSocketAddress(socketFile));
		System.out.println("server: " + server);

		final ExecutorService executor = Executors.newFixedThreadPool(SERVER_INSTANCES);

		for (int i = 0; i < SERVER_INSTANCES; i++) {
			executor.submit(new Runnable() {
				@Override
				public void run() {
					ObjectMapper mapper = new ObjectMapper(new MessagePackFactory());

					while ((!stopped) && (!Thread.interrupted())) {
						try {
							Socket sock = server.accept();
							InputStream is = sock.getInputStream();
							DataInputStream dis = new DataInputStream(is);
							OutputStream os = sock.getOutputStream();

							try {
								// Continute receiving data while socket is open
								while ((!stopped) && (!sock.isClosed())) {

									int messageSize = dis.readInt();
									if ((messageSize <= 0) || (messageSize > MAX_MESSAGE_SIZE)) {
										throw new IOException("Invalid message length " + messageSize);
									}

									byte[] data = new byte[messageSize];
									int bytesRead = is.read(data);
									if (bytesRead != messageSize) {
										throw new IOException("Expected " + messageSize + " bytes, received " + bytesRead);
									}
									JsonNode read = mapper.readTree(data);

									JsonNode topic = read.get(TOPIC_KEY);
									if (topic == null) {
										os.write(BAD_REQUEST);
										os.flush();
									} else {
										String topicName = StringUtils.strip(topic.toString(), "\"").toLowerCase();

										if (Producer.GetInstance().SendMessage(topicName, read, data)) {
											os.write(RESPONSE_OK);
										} else {
											os.write(ERROR);
										}
										os.flush();
									}
								}
							} catch (IOException ex) {
								ex.printStackTrace();
								try {
									os.write(ERROR);
									os.flush();
								} catch (Exception e) {
								}
							} finally {
								if (is != null) {
									is.close();
								}
								if (os != null) {
									os.close();
								}
								sock.close();
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			});
		}

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				stopped = true;
				executor.shutdownNow();
				try {
					server.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					executor.awaitTermination(20000, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Producer.GetInstance().ShutDown();
			}
		});
	}
}
