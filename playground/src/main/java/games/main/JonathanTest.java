package games.main;

import com.google.api.ads.adwords.axis.factory.AdWordsServices;
import com.google.api.ads.adwords.axis.v201506.cm.Location;
import com.google.api.ads.adwords.axis.v201506.cm.Money;
import com.google.api.ads.adwords.axis.v201506.cm.NetworkSetting;
import com.google.api.ads.adwords.axis.v201506.cm.Paging;
import com.google.api.ads.adwords.axis.v201506.o.*;
import com.google.api.ads.adwords.lib.client.AdWordsSession;
import com.google.api.ads.common.lib.auth.OfflineCredentials;
import com.google.api.ads.common.lib.utils.Maps;
import com.google.api.client.auth.oauth2.Credential;
import kafka.consumer.*;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.MessageAndMetadata;
import org.apache.commons.io.output.CountingOutputStream;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.GZIPOutputStream;

public class JonathanTest {
	private static void send(JsonNode result, ObjectMapper om) throws IOException {
		URL u = new URL("http://fcagent-a01.sg.internal:3000/hs/submit");
		HttpURLConnection con = (HttpURLConnection)u.openConnection();
		for (int i = 0; i < 3; i++) {
			try {
				Thread.sleep(5000);
				con.setConnectTimeout(15000);
				con.setReadTimeout(15000);
				con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
				con.setUseCaches(false);
				con.setDoInput(true);
				con.setDoOutput(true);
				String output = om.writeValueAsString(result);
				con.setRequestProperty("Content-Length", Integer.toString(output.getBytes().length));
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(output);
				wr.flush();
				wr.close();
				int code = con.getResponseCode();
				if (code != 200) {
					System.out.println("BLA - " + code);
					System.out.println(con.getResponseMessage());
					System.out.println(result.toString());
					con.disconnect();
					continue;
				}
				con.disconnect();
				break;
			} catch (Exception ex) {
				System.out.println("NET:" + ex.toString());
				if (con != null) {
					con.disconnect();
				}
			}
		}
	}

	// see this How-to for a faster way to convert
	// a byte array to a HEX string
	public static String getMD5Checksum(byte[] digest) throws Exception {
		String result = "";

		for (int i=0; i < digest.length; i++) {
			result += Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
		}
		return result;
	}

	public static void main(String[] args) throws Exception {

		BufferedReader br = new BufferedReader(new FileReader("c:\\-tmp-jonathan-keys-part-r-00000"));
		Credential c = null;
		try {
		 c = new OfflineCredentials.Builder()
					.forApi(OfflineCredentials.Api.ADWORDS)
					.withClientSecrets("305322129069-7mrtni9jj3bduq5qc31f2pfi397ufn9t.apps.googleusercontent.com", "6HsSBXnse4JynuKd7jmfhgIC")
					.withRefreshToken("1/wUeFDdKTn_g1avy9MLe-6xoCWiHxx8ZVvIf7wPdZx_U")
					.build().generateCredential();
			String acc = c.getAccessToken();
			AdWordsSession sess = new AdWordsSession.Builder()
					.fromFile()
					.withDeveloperToken("id9aYtd-Z_oqbtG-vVNQAw")
					.withClientCustomerId("624-387-1455")
					.withUserAgent("Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36")
					.withOAuth2Credential(c).build();
//
			TargetingIdeaServiceInterface targetingIdeaService =
					new AdWordsServices().get(sess, TargetingIdeaServiceInterface.class);

			TargetingIdeaSelector ideaSelector = new TargetingIdeaSelector();
			ideaSelector.setRequestType(RequestType.STATS);
			ideaSelector.setIdeaType(IdeaType.KEYWORD);
			ideaSelector.setRequestedAttributeTypes(new AttributeType[]{AttributeType.KEYWORD_TEXT, AttributeType.AVERAGE_CPC, AttributeType.TARGETED_MONTHLY_SEARCHES, AttributeType.SEARCH_VOLUME});
			LocationSearchParameter loc = new LocationSearchParameter();
			Location us = new Location();
			us.setId(2840l);
			loc.setLocations(new Location[]{us});

			// Set the network.
			NetworkSearchParameter networkSearchParameter =
					new NetworkSearchParameter();

			NetworkSetting networkSetting = new NetworkSetting();
			networkSetting.setTargetGoogleSearch(true);
			networkSetting.setTargetSearchNetwork(false);
			networkSetting.setTargetContentNetwork(false);
			networkSetting.setTargetPartnerSearchNetwork(false);

			networkSearchParameter.setNetworkSetting(networkSetting);

			// Set selector paging (required for targeting idea service).
			Paging paging = new Paging();
			paging.setStartIndex(0);
			paging.setNumberResults(1000);
			ideaSelector.setPaging(paging);

			int requests = 0;
			Set<String> keywords = new HashSet<String>();
			String line;
			while ((line = br.readLine()) != null) {
				keywords.add(StringEscapeUtils.escapeXml(line.trim().replace('\0', ' ')));

				if (keywords.size() == 800) {
					RelatedToQuerySearchParameter rel = new RelatedToQuerySearchParameter();
					rel.setQueries(keywords.toArray(new String[keywords.size()]));
					ideaSelector.setSearchParameters(new SearchParameter[]{rel, loc, networkSearchParameter});

					TargetingIdeaPage data = targetingIdeaService.get(ideaSelector);
					// Display related keywords.
					if (data.getEntries() != null && data.getEntries().length > 0) {
						for (TargetingIdea targetingIdea : data.getEntries()) {
							Map<AttributeType, Attribute> datas = Maps.toMap(targetingIdea.getData());
							StringAttribute keywordsd = (StringAttribute) datas.get(AttributeType.KEYWORD_TEXT);

							Long averageMonthlySearches =
									((LongAttribute) datas.get(AttributeType.SEARCH_VOLUME))
											.getValue();
							Money avgCPC =
									((MoneyAttribute) datas.get(AttributeType.AVERAGE_CPC)).getValue();

							MonthlySearchVolumeAttribute monthly =
									((MonthlySearchVolumeAttribute) datas.get(AttributeType.TARGETED_MONTHLY_SEARCHES));
							StringBuilder sb = new StringBuilder();
							for (MonthlySearchVolume month : monthly.getValue()) {
								sb.append(month.getCount()).append(", ");
							}
//								 System.out.println(keywordsd.getValue() + " : Volume: " + averageMonthlySearches + ", CPC: " + (avgCPC == null ? "null" : avgCPC.getMicroAmount()) + ", Monthly: " + sb.toString());
						}
					}
					requests++;
					System.out.println("Requests: " + requests);
					keywords = new HashSet<String>();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		System.exit(0);
//
//		Credential c = new OfflineCredentials.Builder()
//				.forApi(OfflineCredentials.Api.ADWORDS)
//				.withClientSecrets("305322129069-7mrtni9jj3bduq5qc31f2pfi397ufn9t.apps.googleusercontent.com","6HsSBXnse4JynuKd7jmfhgIC")
//				.withRefreshToken("1/wUeFDdKTn_g1avy9MLe-6xoCWiHxx8ZVvIf7wPdZx_U")
//				.build().generateCredential();
//		String acc = c.getAccessToken();
//		AdWordsSession sess = new AdWordsSession.Builder()
//				.fromFile()
//				.withDeveloperToken("id9aYtd-Z_oqbtG-vVNQAw")
//				.withClientCustomerId("624-387-1455")
//				.withUserAgent("Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36")
//				.withOAuth2Credential(c).build();
//
//		TargetingIdeaServiceInterface targetingIdeaService =
//				new AdWordsServices().get(sess, TargetingIdeaServiceInterface.class);
//
//		TargetingIdeaSelector ideaSelector = new TargetingIdeaSelector();
//		ideaSelector.setRequestType(RequestType.STATS);
//		ideaSelector.setIdeaType(IdeaType.KEYWORD);
//		ideaSelector.setRequestedAttributeTypes(new AttributeType[]{AttributeType.KEYWORD_TEXT, AttributeType.AVERAGE_CPC, AttributeType.TARGETED_MONTHLY_SEARCHES, AttributeType.SEARCH_VOLUME});
//		RelatedToQuerySearchParameter rel = new RelatedToQuerySearchParameter();
//		LocationSearchParameter loc = new LocationSearchParameter();
//		Location us = new Location();
//		us.setId(2250l);
//		loc.setLocations(new Location[]{us});
//
//		// Set the network.
//		NetworkSearchParameter networkSearchParameter =
//				new NetworkSearchParameter();
//
//		NetworkSetting networkSetting = new NetworkSetting();
//		networkSetting.setTargetGoogleSearch(true);
//		networkSetting.setTargetSearchNetwork(false);
//		networkSetting.setTargetContentNetwork(false);
//		networkSetting.setTargetPartnerSearchNetwork(false);
//
//		networkSearchParameter.setNetworkSetting(networkSetting);
//
//		rel.setQueries(new String[] {"britney spears", "book a flight", "nike", "car insurance", "vincent cassel"});
//		ideaSelector.setSearchParameters(new SearchParameter[] {rel,loc, networkSearchParameter});
//
//		// Set selector paging (required for targeting idea service).
//		Paging paging = new Paging();
//		paging.setStartIndex(0);
//		paging.setNumberResults(1000);
//		ideaSelector.setPaging(paging);
//
//		TargetingIdeaPage data = targetingIdeaService.get(ideaSelector);
//		// Display related keywords.
//		if (data.getEntries() != null && data.getEntries().length > 0) {
//			for (TargetingIdea targetingIdea : data.getEntries()) {
//				Map<AttributeType, Attribute> datas = Maps.toMap(targetingIdea.getData());
//				StringAttribute keywordsd = (StringAttribute) datas.get(AttributeType.KEYWORD_TEXT);
//
//				Long averageMonthlySearches =
//						((LongAttribute) datas.get(AttributeType.SEARCH_VOLUME))
//								.getValue();
//				Money avgCPC =
//						((MoneyAttribute) datas.get(AttributeType.AVERAGE_CPC)).getValue();
//
//				MonthlySearchVolumeAttribute monthly =
//						((MonthlySearchVolumeAttribute) datas.get(AttributeType.TARGETED_MONTHLY_SEARCHES));
//				StringBuilder sb = new StringBuilder();
//				for (MonthlySearchVolume month : monthly.getValue()){
//					sb.append(month.getCount()).append(", ");
//				}
//				System.out.println(keywordsd.getValue() + " : Volume: " + averageMonthlySearches + ", CPC: " + (avgCPC == null ? "null" : avgCPC.getMicroAmount()) + ", Monthly: " + sb.toString());
//			}
//		}
//
//		TrafficEstimatorServiceInterface  sss = new AdWordsServices().get(sess, TrafficEstimatorServiceInterface .class);
//
//		List<Keyword> keywords = new ArrayList<Keyword>();
//		Keyword k = new Keyword();
//		k.setText("britney spears");
//		k.setMatchType(KeywordMatchType.EXACT);
//		keywords.add(k);
//		k = new Keyword();
//		k.setText("book a flight");
//		k.setMatchType(KeywordMatchType.EXACT);
//		keywords.add(k);
//		k = new Keyword();
//		k.setText("nike");
//		k.setMatchType(KeywordMatchType.EXACT);
//		keywords.add(k);
//		k = new Keyword();
//		k.setText("car insurance");
//		k.setMatchType(KeywordMatchType.EXACT);
//		keywords.add(k);
//		k = new Keyword();
//		k.setText("vincent cassel");
//		k.setMatchType(KeywordMatchType.EXACT);
//		keywords.add(k);
//
//		// Create a keyword estimate request for each keyword.
//		List<KeywordEstimateRequest> keywordEstimateRequests = new ArrayList<KeywordEstimateRequest>();
//		for (Keyword keyword : keywords) {
//			KeywordEstimateRequest keywordEstimateRequest = new KeywordEstimateRequest();
//			keywordEstimateRequest.setKeyword(keyword);
//			keywordEstimateRequests.add(keywordEstimateRequest);
//		}
//
//		// Create ad group estimate requests.
//		List<AdGroupEstimateRequest> adGroupEstimateRequests = new ArrayList<AdGroupEstimateRequest>();
//		AdGroupEstimateRequest adGroupEstimateRequest = new AdGroupEstimateRequest();
//		adGroupEstimateRequest.setKeywordEstimateRequests(keywordEstimateRequests
//				.toArray(new KeywordEstimateRequest[]{}));
//		adGroupEstimateRequest.setMaxCpc(new Money(null, 1000000L));
//		adGroupEstimateRequests.add(adGroupEstimateRequest);
//
//		// Create campaign estimate requests.
//		List<CampaignEstimateRequest> campaignEstimateRequests =
//				new ArrayList<CampaignEstimateRequest>();
//		CampaignEstimateRequest campaignEstimateRequest = new CampaignEstimateRequest();
//		campaignEstimateRequest.setAdGroupEstimateRequests(adGroupEstimateRequests
//				.toArray(new AdGroupEstimateRequest[] {}));
//		campaignEstimateRequest.setNetworkSetting(new NetworkSetting(true, false, false, false));
//		campaignEstimateRequests.add(campaignEstimateRequest);
//
//		// Create selector.
//		TrafficEstimatorSelector selector = new TrafficEstimatorSelector();
//		selector.setCampaignEstimateRequests(campaignEstimateRequests.toArray(new CampaignEstimateRequest[]{}));
//
//		TrafficEstimatorResult result = sss.get(selector);
//		// Display traffic estimates.
//		if (result != null && result.getCampaignEstimates() != null) {
//			KeywordEstimate[] keywordEstimates =
//					result.getCampaignEstimates()[0].getAdGroupEstimates()[0].getKeywordEstimates();
//			for (int i = 0; i < keywordEstimates.length; i++) {
//				Keyword keyword = keywordEstimateRequests.get(i).getKeyword();
//				KeywordEstimate keywordEstimate = keywordEstimates[i];
//				System.out.println(keyword.getText() + " : CPC: " + (keywordEstimate.getMin().getAverageCpc() == null ? "0": keywordEstimate.getMin().getAverageCpc().getMicroAmount()) + " - " +
//						(keywordEstimate.getMax().getAverageCpc() == null ? "0": keywordEstimate.getMax().getAverageCpc().getMicroAmount()));
//				if (Boolean.TRUE.equals(keywordEstimateRequests.get(i).getIsNegative())) {
//					continue;
//				}
//			}
//		} else {
//			System.out.println("No traffic estimates were returned.");
//		}


		Configuration configuration = new Configuration();
		configuration.set("fs.hdfs.impl",
				org.apache.hadoop.hdfs.DistributedFileSystem.class.getName()
		);
		configuration.set("fs.file.impl",
				org.apache.hadoop.fs.LocalFileSystem.class.getName()
		);
		System.out.println(configuration.get("fs.defaultFS"));
		if (true) return;

		MessageDigest md = MessageDigest.getInstance("MD5");
		FileSystem hdfs = FileSystem.get( new URI( "hdfs://hd-nn-a01.sg.internal:8020" ), configuration );
		Path file = new Path("hdfs://hd-nn-a01.sg.internal:8020/tmp/jonathan/bla.txt.gz");
		if ( hdfs.exists( file )) { hdfs.delete( file, true ); }
		OutputStream os = hdfs.create( file);
		DigestOutputStream dis = new DigestOutputStream(os, md);
		CountingOutputStream count = new CountingOutputStream(dis);
		GZIPOutputStream gzip = new GZIPOutputStream(count);
		BufferedWriter bscr = new BufferedWriter( new OutputStreamWriter( gzip, "UTF-8" ) );
		bscr.write("Hello World");
		bscr.close();

		System.out.println(count.getByteCount());

		byte[] digest = md.digest();
		Path check = new Path("hdfs://hd-nn-a01.sg.internal:8020/tmp/jonathan/bla.txt.gz.md5");
		if ( hdfs.exists( check )) { hdfs.delete( check, true ); }
		os = hdfs.create( check);
		bscr = new BufferedWriter( new OutputStreamWriter( os, "UTF-8" ) );
		bscr.write(getMD5Checksum(digest) + "  bla.txt.gz");
		bscr.close();
		hdfs.close();

		long i= 0x2eb627c846010000l;
		i = (i & 0x00ff00ff00ff00ffL) << 8 | (i >>> 8) & 0x00ff00ff00ff00ffL;
		long b = (i << 48) | ((i & 0xffff0000L) << 16) |
				((i >>> 16) & 0xffff0000L) | (i >>> 48);

		String url = "https://www.quantcast.com/top-sites/US/%v";
		Properties props = new Properties();
		props.put("zookeeper.connect", "kafka-a01.sg.internal:2181,kafka-a02.sg.internal:2181,kafka-a03.sg.internal:2181,kafka-a04.sg.internal:2181,kafka-a05.sg.internal:2181/kafka/trackers");
		props.put("group.id", "test");
		props.put("zookeeper.session.timeout.ms", "400");
		props.put("zookeeper.sync.time.ms", "200");
		props.put("auto.commit.interval.ms", "1000");
		props.put("consumer.timeout.ms", "1000");
		props.put("auto.offset.reset", "smallest");
		ConsumerConfig conf = new ConsumerConfig(props);

		ExecutorService executor = Executors.newFixedThreadPool(1);

		ConsumerConnector cons = Consumer.createJavaConsumerConnector(conf);

		Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
		topicCountMap.put("Tracker", 1);
		Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = cons.createMessageStreams(topicCountMap);
		List<KafkaStream<byte[], byte[]>> streams = consumerMap.get("Tracker");
		List<ConsumerTest> ths = new ArrayList<ConsumerTest>();
		for (int z = 0; z < 1; z++) {
			KafkaStream<byte[], byte[]> st = streams.get(z);
			ConsumerTest th = new ConsumerTest(st);
			ths.add(th);
			executor.submit(th);
		}

		Thread.sleep(20000);
		cons.shutdown();
		for (ConsumerTest th : ths){
			th.stop = true;
		}
		executor.shutdown();
		System.out.println("end");
	}

	public static class ConsumerTest implements Runnable {
		private KafkaStream<byte[], byte[]> m_stream;

		public boolean stop = false;
		public ConsumerTest(KafkaStream<byte[], byte[]> a_stream) {
			m_stream = a_stream;
		}

		public void run() {
			System.out.println("Starting");
			while (!stop) {
				try {
					ConsumerIterator<byte[], byte[]> it = m_stream.iterator();
					while (it.hasNext()) {
						System.out.println("!");
						MessageAndMetadata<byte[], byte[]> message = it.next();
//						System.out.println("Key: " + new String(message.key()));
//						MessagePack msgpack = new MessagePack();
//						Value data = msgpack.read(message.message());
//						System.out.println("Value: " + data.toString());
						System.out.println("----------------------");
						String input = System.console().readLine();
					}
				} catch (ConsumerTimeoutException ex) {
					System.out.print(".");}
				catch (Exception ex){
					ex.printStackTrace();
				}
			}

			System.out.println("Stopped");
		}
	}
}
