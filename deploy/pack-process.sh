#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TARGETFOLDER=${DIR}/../process
PACKAGES={DIR}/../package/*.tar.gz
TMP=/tmp/package-process/$$

while [[ $# > 0 ]]
do
key="$1"
shift
case $key in
    -t|--targetfolder)
    TARGETFOLDER="$1"
    shift
    ;;
    -f|--files)
    PACKAGES="$(echo $1| tr ',' ' ')"
    shift
    ;;
    -s|--salt)
    SALT="$1"
    shift
    ;;
    -dt|--deploytype)
    DEPLOYTYPE="$1"
    shift
    ;;
    -p|--process)
    PROCESS="$1"
    shift
    ;;
    -v|--version)
    VERSION="$1"
    shift
    ;;
    *)
    echo "USAGE: ${BASH_SOURCE[0]}"
    echo "  [-p,--packages <packages to include>] - [required]"
    echo "  [-s,--salt <salt type>] - [required]"
    echo "  [-t,--targetfolder <target folder>] - Default: 'process'"
    echo "  [-v,--version] - [recuired]"
    exit 2
    ;;
esac
done

echo Building process ${PROCESS}...

mkdir -p ${TARGETFOLDER}
mkdir -p ${TMP}
echo salt=${SALT} > ${TMP}/deploy.properties
echo process=${PROCESS} >> ${TMP}/deploy.properties
echo deploytype=${DEPLOYTYPE} >> ${TMP}/deploy.properties
echo version=${VERSION} >> ${TMP}/deploy.properties
old=`pwd`
cd ${TMP} && tar cf ${TARGETFOLDER}/${PROCESS}.${VERSION}.tar *
cd ${old}
rm ${TMP}/*
for f in ${PACKAGES}
do
  dir=$(dirname ${f})
  file=$(basename ${f})
  echo ${file}
  cd ${dir}
  if [ "$DEPLOYTYPE" != "war" ];then
      prefix=`echo ${file} | cut -d'.' -f 1`
      rm -rf ${TMP}/${prefix}
  fi

  mkdir -p  ${TMP}/${prefix}
  echo "Unpacking tarball..."
  tar -xvf ${file} -C  ${TMP}/${prefix} > /dev/null
  if [ "$DEPLOYTYPE" != "war" ];then
    cd ${TMP}
    tar rf ${TARGETFOLDER}/${PROCESS}.${VERSION}.tar ${prefix}
  else
      cd ${TMP}/${prefix}
      tar rf ${TARGETFOLDER}/${PROCESS}.${VERSION}.tar *
  fi
  cd ${old}
  pushd ${TMP}/${prefix} > /dev/null
  tar rf ${TARGETFOLDER}/${PROCESS}.${VERSION}.tar.gz *
  popd  > /dev/null
  rm -rf  ${TMP}/${prefix}
done
