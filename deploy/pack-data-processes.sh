#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

prefix=${DIR}/../package
declare -A processes
declare -a parts
declare -a files


while [[ $# > 0 ]]
do
key="$1"
shift
case $key in
    --gitversion)
    gitversion="$1"
    shift
    ;;
    --build)
    build=$1
    shift
    ;;
    *)
    ;;
esac
done



. ${DIR}/../build.processes

if [ "$gitversion" == "" ];then
    gitversion=$(git log -1 --pretty=format:%h)
fi
if [ "$build" == "" ];then
    build=42
fi
version=1.2.${build}.${gitversion}
declare -a teamcity=()
declare -i counter=0
teamcity+=("select")
teamcity+=("display='prompt'")
teamcity+=("label='Process To Deploy'")
for process in ${!processes[@]} ;do
    parts=()
    files=()
    parts=($(echo ${processes[$process]}|tr '+' ' '  ))
    for package in $(echo ${parts[2]}|tr ',' ' ') ;do
        files+=("$prefix/${package}.${version}.tar.gz")
    done
    teamcity+=("label_$counter='$process'")
    teamcity+=("data_$counter='process/$process.%dep.SpidersKafka_Server_Ondemand.build.number%.tar'")
    ${DIR}/pack-process.sh -v ${version} -f $(echo ${files[@]} | tr ' ' ',') -p ${process} -s ${parts[0]} -dt ${parts[1]}
    let counter+=1
done

cat > ${DIR}/entity.xml << EOF
<property name="process.artefacts" value="" own="true">
<type rawValue="${teamcity[@]}"/>
</property>
EOF

curl -s -X POST -u 'teamcity:teamcity' -H 'Content-Type:application/xml' --data @entity.xml http://teamcity.sg.internal/httpAuth/app/rest/buildTypes/SpidersKafka_DeployFromSpecificBranch/parameters

#${team.city}/httpAuth/app/rest/buildTypes/Bigdata_DeployFromSpecificBranch/parameters
